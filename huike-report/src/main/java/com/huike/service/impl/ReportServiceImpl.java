package com.huike.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.huike.business.mapper.TbBusinessMapper;
import com.huike.clues.domain.TbActivity;
import com.huike.clues.domain.TbClue;
import com.huike.clues.mapper.*;
import com.huike.common.core.domain.model.SysDept;
import com.huike.contract.domain.TbContract;
import com.huike.contract.mapper.TbContractMapper;
import com.huike.report.domain.vo.*;
import com.huike.report.mapper.ReportMapper;
import com.huike.report.result.ReportActivityStatisticsData;
import com.huike.report.result.ReportChannelStatisticsData;
import com.huike.service.ReportService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    private TbContractMapper tbContractMapper;
    @Autowired
    private SysDictDataMapper sysDictDataMapper;
    @Autowired
    private ReportMapper reportMapper;
    @Autowired
    private SysDeptMapper deptMapper;
    @Autowired
    private TbCourseMapper courseMapper;
    @Autowired
    private TbClueMapper clueMapper;
    @Autowired
    private TbBusinessMapper businessMapper;
    @Autowired
    private TbActivityMapper activityMapper;
    @Autowired
    private TbContractMapper contractMapper;

    @Override
    public LineChartVo contractStatistics(LocalDate beginCreateTime, LocalDate endCreateTime) {
        List<LocalDate> dateList = new ArrayList<>();
        dateList.add(beginCreateTime);
        while (!beginCreateTime.equals(endCreateTime)) {
            beginCreateTime = beginCreateTime.plusDays(1);
            dateList.add(beginCreateTime);
        }
        List<Integer> newUserList = new ArrayList<>(); // 新增用户数
        List<Integer> totalUserList = new ArrayList<>(); // 总用户数
        List<String> xAxis = new ArrayList<>();
        for (LocalDate date : dateList) {
            xAxis.add(date.toString());
            LocalDateTime beginTime = LocalDateTime.of(date, LocalTime.MIN);
            LocalDateTime endTime = LocalDateTime.of(date, LocalTime.MAX);
            Integer newUser = getUserCount(beginTime, endTime);
            Integer totalUser = getUserCount(null, endTime);
            newUserList.add(newUser);
            totalUserList.add(totalUser);
        }
        LineSeriesVo newUserSeries = new LineSeriesVo();
        newUserSeries.setName("新增用户数");
        newUserSeries.setData(newUserList);
        LineSeriesVo totalUserSeries = new LineSeriesVo();
        totalUserSeries.setName("总用户数");
        totalUserSeries.setData(totalUserList);
        List<LineSeriesVo> series = new ArrayList<>();
        series.add(newUserSeries);
        series.add(totalUserSeries);
        LineChartVo lineChartVo = new LineChartVo();
        lineChartVo.setxAxis(xAxis);
        lineChartVo.setSeries(series);
        return lineChartVo;
    }

    @Override
    public Page<TbContract> contractReportList(Integer pageNum,
                                               Integer pageSize,
                                               LocalDate beginCreateTime,
                                               LocalDate endCreateTime,
                                               String channel,
                                               String createBy,
                                               String deptId) {
        PageHelper.startPage(pageNum, pageSize);
        List<TbContract> list = tbContractMapper.getList(beginCreateTime, endCreateTime, channel, createBy, deptId);
        for (TbContract tbContract : list) {
            Long courseId = tbContract.getCourseId();
            if (courseId != null) {
                String courseName = courseMapper.selectCourseNameById(courseId);
                tbContract.setCourseName(courseName);
            }
        }
        return (Page) list;
    }

    @Override
    public List<Map<String, Object>> subjectStatistics(String beginCreateTime,
                                                       String endCreateTime) {
        List<Map<String, Object>> mapList = tbContractMapper.subjectStatistics(beginCreateTime, endCreateTime);
        for (Map<String, Object> map : mapList) {
            String subject = (String) map.get("subject");
            String label = sysDictDataMapper.selectDictLabel("course_subject", subject);
            map.put("subject", label);
        }
        return mapList;
    }

    @Override
    public LineChartVo salesStatistics(LocalDate beginCreateTime, LocalDate endCreateTime) {
        List<String> xAxis = new ArrayList<>();
        List<LocalDate> dateList = new ArrayList<>();
        dateList.add(beginCreateTime);
        while (!beginCreateTime.equals(endCreateTime)) {
            beginCreateTime = beginCreateTime.plusDays(1);
            dateList.add(beginCreateTime);
        }
        LineChartVo lineChartVo = new LineChartVo();
        List<LineSeriesVo> lineSeriesVoList = new ArrayList<>();
        LineSeriesVo lineSeriesVo = new LineSeriesVo();
        List<Integer> dailySales = new ArrayList<>();
        for (LocalDate date : dateList) {
            xAxis.add(date.toString());
            LocalDateTime beginTime = LocalDateTime.of(date, LocalTime.MIN);
            LocalDateTime endTime = LocalDateTime.of(date, LocalTime.MAX);
            List<Integer> sales = tbContractMapper.getSales(beginTime, endTime);
            System.out.println(sales);
            if (sales.get(0) != null) {
                dailySales.add(sales.get(0));
            } else {
                dailySales.add(0);
            }
        }
        lineSeriesVo.setName("销售统计");
        lineSeriesVo.setData(dailySales);
        lineSeriesVoList.add(lineSeriesVo);
        lineChartVo.setxAxis(xAxis);
        lineChartVo.setSeries(lineSeriesVoList);
        return lineChartVo;
    }

    @Override
    public Page<Map<String, Object>> deptStatisticsList(Integer pageNum, Integer pageSize, LocalDate beginCreateTime, LocalDate endCreateTime) {
        PageHelper.startPage(pageNum, pageSize);
        List<Map<String, Object>> list = tbContractMapper.getDeptStatisticsList(beginCreateTime, endCreateTime);
        Page page = (Page) list;
        for (Map<String, Object> map : list) {
            Long deptId = (Long) map.get("dept_id");
            if (deptId != null) {
                SysDept dept = deptMapper.selectDeptById(deptId);
                map.put("deptName", dept.getDeptName());
            }
        }
        return page;
    }

    @Override
    public List<Map<String, Object>> channelStatisticsList(LocalDate beginCreateTime, LocalDate endCreateTime) {
        List<Map<String, Object>> list = tbContractMapper.channelStatisticsList(beginCreateTime, endCreateTime);
        for (Map<String, Object> map : list) {
            String channel = (String) map.get("channel");
            if (channel != null) {
                String label = sysDictDataMapper.selectDictLabel("clues_item", channel);
                map.put("channel", label);
            }
        }
        return list;
    }

    @Override
    public List<Map<String, Object>> ownerShipStatistics(LocalDate beginCreateTime, LocalDate endCreateTime) {
        return tbContractMapper.ownerShipStatistics(beginCreateTime, endCreateTime);

    }

    @Override
    public LineChartVo cluesStatistics(LocalDate beginCreateTime, LocalDate endCreateTime) {
        List<LocalDate> dateList = new ArrayList<>();
        dateList.add(beginCreateTime);
        while (!beginCreateTime.equals(endCreateTime)) {
            beginCreateTime = beginCreateTime.plusDays(1);
            dateList.add(beginCreateTime);
        }
        List<Integer> newClueList = new ArrayList<>(); // 新增用户数
        List<Integer> totalClueList = new ArrayList<>(); // 总用户数
        List<String> xAxis = new ArrayList<>();
        for (LocalDate date : dateList) {
            xAxis.add(date.toString());
            LocalDateTime beginTime = LocalDateTime.of(date, LocalTime.MIN);
            LocalDateTime endTime = LocalDateTime.of(date, LocalTime.MAX);
            Integer newClue = clueMapper.countClueByTime(beginTime, endTime);
            Integer totalClue = clueMapper.countClueByTime(null, endTime);
            newClueList.add(newClue);
            totalClueList.add(totalClue);
        }
        LineSeriesVo newClueSeries = new LineSeriesVo();
        newClueSeries.setName("新增线索数");
        newClueSeries.setData(newClueList);
        LineSeriesVo totalClueSeries = new LineSeriesVo();
        totalClueSeries.setName("线索总数量");
        totalClueSeries.setData(totalClueList);
        List<LineSeriesVo> series = new ArrayList<>();
        series.add(newClueSeries);
        series.add(totalClueSeries);
        LineChartVo lineChartVo = new LineChartVo();
        lineChartVo.setxAxis(xAxis);
        lineChartVo.setSeries(series);
        return lineChartVo;
    }

    @Override
    public List<ReportClueDTO> cluesStatisticsList(ReportClueDTO reportClueDTO) {
        List<ReportClueDTO> list = reportMapper.cluesStatisticsList(reportClueDTO);
        return list;
    }

    @Override
    public VulnerabilityMapVo getVulnerabilityMap(LocalDate beginCreateTime, LocalDate endCreateTime) {
        VulnerabilityMapVo vulnerabilityMapVo = new VulnerabilityMapVo();
        LocalDateTime begin = LocalDateTime.of(beginCreateTime, LocalTime.MIN);
        LocalDateTime end = LocalDateTime.of(endCreateTime, LocalTime.MAX);
        Integer clueCount = clueMapper.countClueByTime(begin, end);
        Integer contractCount = tbContractMapper.countByTime(begin, end);
        Integer businessCount = businessMapper.getBusinessCount(begin, end);
        Integer usefulClueCount = clueMapper.countUsefulClueByTime(begin, end);
        vulnerabilityMapVo.setCluesNums(clueCount);
        vulnerabilityMapVo.setBusinessNums(businessCount);
        vulnerabilityMapVo.setContractNums(contractCount);
        vulnerabilityMapVo.setEffectiveCluesNums(usefulClueCount);
        return vulnerabilityMapVo;
    }

    @Override
    public List<ReportChannelStatisticsData> chanelStatistics(LocalDate beginCreateTime, LocalDate endCreateTime) {
        LocalDateTime begin = LocalDateTime.of(beginCreateTime, LocalTime.MIN);
        LocalDateTime end = LocalDateTime.of(endCreateTime, LocalTime.MAX);
        List<ReportChannelStatisticsData> reportChannelStatisticsDataList = new ArrayList<>();
        ReportChannelStatisticsData reportChannelStatisticsData = new ReportChannelStatisticsData();
        List<Map<String, Object>> list = tbContractMapper.chanelStatistics(begin, end);
        for (Map<String, Object> map : list) {
            String channel = (String) map.get("channel");
            if (channel != null) {
                String label = sysDictDataMapper.selectDictLabel("clues_item", channel);
                if (label != null) {
                    reportChannelStatisticsData.setChannel(label);
                    reportChannelStatisticsData.setNum(Math.toIntExact((Long) map.get("num")));
                    reportChannelStatisticsDataList.add(reportChannelStatisticsData);
                }
            }
        }
        return reportChannelStatisticsDataList;
    }

    @Override
    public List<ReportActivityStatisticsData> activityStatistics(LocalDate beginCreateTime, LocalDate endCreateTime) {
        List<ReportActivityStatisticsData> reportActivityStatisticsDataList = new ArrayList<>();
        List<Map<String, Object>> list = tbContractMapper.activityStatistics(beginCreateTime.toString(), endCreateTime.toString());
        for (Map<String, Object> map : list) {
        ReportActivityStatisticsData reportActivityStatisticsData = new ReportActivityStatisticsData();
            Long activityId = (Long) map.get("activity_id");
            if (activityId != null) {
                reportActivityStatisticsData.setActivityId(Integer.parseInt(activityId.toString()));
                String label = activityMapper.selectActivityNameById(Integer.parseInt(activityId.toString()));
                reportActivityStatisticsData.setActivity(label);
                reportActivityStatisticsData.setNum(Math.toIntExact((Long) map.get("num")));
            }if (activityId == null){
                reportActivityStatisticsData.setActivity("其他");
                reportActivityStatisticsData.setNum(Math.toIntExact((Long) map.get("num")));
                reportActivityStatisticsData.setActivityId(-1);
            }
            reportActivityStatisticsDataList.add(reportActivityStatisticsData);
        }
        return reportActivityStatisticsDataList;
    }


    public List<ReportActivityStatisticsData> activityStatisticsList(LocalDate beginCreateTime, LocalDate endCreateTime) {
        List<Map<String, Object>> list = tbContractMapper.activityStatistics(beginCreateTime.toString(), endCreateTime.toString());
        return null;
    }


    private Integer getUserCount(LocalDateTime beginTime, LocalDateTime endTime) {
        Map map = new HashMap();
        map.put("begin", beginTime);
        map.put("end", endTime);
        return tbContractMapper.countByMap(map);
    }

    @Override
    public List<ActivityStatisticsVo> activityStatisticsList(TbActivity query) {
        query.setStatus("2");
        List<TbActivity> activities= activityMapper.selectTbActivityList(query);
        Map<String, Object> timeMap = query.getParams();
        List<ActivityStatisticsVo> list=new ArrayList<>();
        for (TbActivity activity : activities) {
            ActivityStatisticsVo dto = new ActivityStatisticsVo();
            BeanUtils.copyProperties(activity, dto);
            // TbClue tbClue = new TbClue();
            // tbClue.setActivityId(activity.getId());
            // tbClue.setChannel(activity.getChannel());
            // tbClue.setParams(timeMap);
            Map<String, Object> clueCount = clueMapper.countByActivity(query);
            if (clueCount != null) {
                dto.setCluesNum(Integer.parseInt(clueCount.get("total").toString()));
                if(clueCount.get("falseClues")!=null){
                    dto.setFalseCluesNum(Integer.parseInt(clueCount.get("falseClues").toString()));
                }
                if (clueCount.get("toBusiness") != null) {
                    dto.setBusinessNum(Integer.parseInt(clueCount.get("toBusiness").toString()));
                }
            }
            query.setId(activity.getId());
            // TbContract tbContract = new TbContract();
            // tbContract.setChannel(activity.getChannel());
            // tbContract.setActivityId(activity.getId());
            // tbContract.setParams(timeMap);
            Map<String, Object> contractCount = contractMapper.countByActivity(query);
            if (contractCount != null) {
                dto.setCustomersNum(Integer.parseInt(contractCount.get("customersNum").toString()));
                if(contractCount.get("amount")==null) {
                    dto.setAmount(0d);
                }else {
                    dto.setAmount((Double) contractCount.get("amount"));
                }
                if(contractCount.get("cost")==null) {
                    dto.setCost(0d);
                }else {
                    dto.setCost((Double) contractCount.get("cost"));
                }

            }
            list.add(dto);
        }
        return list;
    }
}

