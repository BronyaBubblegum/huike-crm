package com.huike.service;

import com.github.pagehelper.Page;
import com.huike.clues.domain.TbActivity;
import com.huike.contract.domain.TbContract;
import com.huike.report.domain.vo.*;
import com.huike.report.result.ReportActivityStatisticsData;
import com.huike.report.result.ReportChannelStatisticsData;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface ReportService {


        LineChartVo contractStatistics(LocalDate beginCreateTime, LocalDate endCreateTime);


    Page<TbContract> contractReportList(Integer pageNum, Integer pageSize, LocalDate beginCreateTime, LocalDate endCreateTime, String channel, String createBy, String deptId);

    List<Map<String,Object>> subjectStatistics(String beginCreateTime, String endCreateTime);

    LineChartVo salesStatistics(LocalDate beginCreateTime, LocalDate endCreateTime);

    Page<Map<String, Object>> deptStatisticsList(Integer pageNum, Integer pageSize, LocalDate beginCreateTime, LocalDate endCreateTime);

    List<Map<String, Object>> channelStatisticsList(LocalDate beginCreateTime, LocalDate endCreateTime);

    List<Map<String, Object>> ownerShipStatistics(LocalDate beginCreateTime, LocalDate endCreateTime);

    LineChartVo cluesStatistics(LocalDate beginCreateTime, LocalDate endCreateTime);

    public List<ReportClueDTO> cluesStatisticsList(ReportClueDTO reportClueDTO);

    VulnerabilityMapVo getVulnerabilityMap(LocalDate beginCreateTime, LocalDate endCreateTime);

    List<ReportChannelStatisticsData> chanelStatistics(LocalDate beginCreateTime, LocalDate endCreateTime);

    List<ReportActivityStatisticsData> activityStatistics(LocalDate beginCreateTime, LocalDate endCreateTime);

    List<ActivityStatisticsVo> activityStatisticsList(TbActivity activity);
}
