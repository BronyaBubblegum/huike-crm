package com.huike.report.service.impl;

import com.huike.business.mapper.TbBusinessMapper;
import com.huike.clues.domain.vo.IndexStatisticsVo;
import com.huike.clues.mapper.TbClueMapper;
import com.huike.common.utils.SecurityUtils;
import com.huike.contract.mapper.TbContractMapper;
import com.huike.report.domain.vo.IndexBaseInfoVO;
import com.huike.report.domain.vo.IndexTodayInfoVO;
import com.huike.report.domain.vo.IndexTodoInfoVO;
import com.huike.report.service.IndexService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;

/**
 * @Description TODO
 * @Author Louis
 * @Time 2023/12/13 21:08
 */
@Service
public class IndexServiceImpl implements IndexService {

    @Resource
    private TbBusinessMapper tbBusinessMapper;
    @Resource
    private TbClueMapper tbClueMapper;
    @Resource
    private TbContractMapper tbContractMapper;


    @Override
    public List<Map<String,Object>> businessChangeStatistics(IndexStatisticsVo indexStatisticsVo) {

        int allBusiness=  tbBusinessMapper.countAllBusiness(indexStatisticsVo.getBeginCreateTime(),indexStatisticsVo.getEndCreateTime());
        List<Map<String,Object>> list= tbBusinessMapper.countAllContractByUser(indexStatisticsVo);
        for (Map<String, Object> datum : list) {
            Long num= (Long) datum.get("num");
            datum.put("radio",getRadio(allBusiness,num));
        }
        return list;
    }

    private BigDecimal getRadio(Integer all,Long num) {
        if(all.intValue()==0){
            return new BigDecimal(0);
        }
        BigDecimal numBigDecimal = new BigDecimal(num);
        BigDecimal allBigDecimal = new BigDecimal(all);
        BigDecimal divide = numBigDecimal.divide(allBigDecimal,4,BigDecimal.ROUND_HALF_UP);
        return divide.multiply(new BigDecimal(100));
    }

    @Override
    public List<Map<String, Object>> salesStatistic(IndexStatisticsVo indexStatisticsVo) {
        int allClues=  tbClueMapper.countAllClue(indexStatisticsVo.getBeginCreateTime(),indexStatisticsVo.getEndCreateTime());
        List<Map<String,Object>> list= tbClueMapper.countAllClueByUser(indexStatisticsVo);
        for (Map<String, Object> datum : list) {
            Long num= (Long) datum.get("num");
            datum.put("radio",getRadio(allClues,num));
        }
        return list;
    }

    @Override
    public IndexTodayInfoVO getTodayInfo() {
        IndexTodayInfoVO indexTodayInfoVO = new IndexTodayInfoVO();
        // 获取当天的日期
        LocalDate today = LocalDate.now();
        // 获取当天最大时间和最小时间
        LocalDateTime start = today.atStartOfDay();
        LocalDateTime end = today.atTime(LocalTime.MAX);
        String username = SecurityUtils.getUsername();
        // 今日线索数量
        Integer todayBusinessNum = tbBusinessMapper.selectBusinessCount(start, end, username);
        // 今日线索数量
        Integer todayCluesNum = tbClueMapper.selectCluesCount(start, end, username);
        // 今日合同数量
        Integer todayContractNum = tbContractMapper.selectContractCount(start, end, username);
        // 今日营业额
        Double todaySalesAmount = tbContractMapper.selectSalesAmount(start, end, username);
        todaySalesAmount = todaySalesAmount == null ? 0.0 : todaySalesAmount;
        indexTodayInfoVO.setTodayBusinessNum(todayBusinessNum);
        indexTodayInfoVO.setTodayCluesNum(todayCluesNum);
        indexTodayInfoVO.setTodayContractNum(todayContractNum);
        indexTodayInfoVO.setTodaySalesAmount(todaySalesAmount);
        return indexTodayInfoVO;
    }

    @Override
    public IndexTodoInfoVO getTodoInfo(String startTime, String endTime) {
        IndexTodoInfoVO indexTodoInfoVO = new IndexTodoInfoVO();
        LocalDate start = LocalDate.parse(startTime);
        LocalDate end = LocalDate.parse(endTime);
        // 获取当前日期最大时间和最小时间和用户名
        LocalDateTime timeOfStart = start.atStartOfDay();
        LocalDateTime timeOfEnd = end.atTime(LocalTime.MAX);
        String username = SecurityUtils.getUsername();
        // 待跟进线索数目
        Integer toallocatedCluesNum = tbClueMapper.getToallocatedCluesNum(timeOfStart, timeOfEnd, username);
        // 待分配商机数目
        Integer toallocatedBusinessNum = tbBusinessMapper.getToallocatedBusinessNum(timeOfStart, timeOfEnd, username);
        // 待分配线索数目
        Integer tofollowedCluesNum = tbClueMapper.getTofollowedCluesNum(timeOfStart, timeOfEnd, username);
        // 待分配商机数目
        Integer tofollowedBusinessNum = tbBusinessMapper.getTofollowedBusinessNum(timeOfStart, timeOfEnd, username);
        // 封装数据返回
        indexTodoInfoVO.setToallocatedCluesNum(toallocatedCluesNum);
        indexTodoInfoVO.setToallocatedBusinessNum(toallocatedBusinessNum);
        indexTodoInfoVO.setTofollowedCluesNum(tofollowedCluesNum);
        indexTodoInfoVO.setTofollowedBusinessNum(tofollowedBusinessNum);
        return indexTodoInfoVO;
    }

    @Override
    public IndexBaseInfoVO getBaseInfo(String startTime, String endTime) {
        IndexBaseInfoVO result = new IndexBaseInfoVO();
        LocalDate start = LocalDate.parse(startTime);
        LocalDate end = LocalDate.parse(endTime);
        // 获取当天最大时间和最小时间和用户名
        LocalDateTime timeOfStart = start.atStartOfDay();
        LocalDateTime timeOfEnd = end.atTime(LocalTime.MAX);
        String username = SecurityUtils.getUsername();
        // 查询线索数量
        Integer clueCount = tbClueMapper.selectCluesCountAll(timeOfStart, timeOfEnd, username);
        // 查询商机数量
        Integer businessCount = tbBusinessMapper.selectBusinessCountAll(timeOfStart, timeOfEnd, username);
        // 查询合同数量
        Integer contractCount = tbContractMapper.selectContractCountAll(timeOfStart, timeOfEnd, username);
        // 查询营业额
        Double salesAmount = tbContractMapper.selectSalesAmountAll(timeOfStart, timeOfEnd, username);
        // 封装数据
        result.setCluesNum(clueCount);
        result.setBusinessNum(businessCount);
        result.setContractNum(contractCount);
        result.setSalesAmount(salesAmount);
        return result;
    }


}
