package com.huike.report.service;

import com.huike.clues.domain.vo.IndexStatisticsVo;
import com.huike.report.domain.vo.IndexBaseInfoVO;
import com.huike.report.domain.vo.IndexTodayInfoVO;
import com.huike.report.domain.vo.IndexTodoInfoVO;
import java.util.List;
import java.util.Map;

/**
 * @Description TODO
 * @Author Louis
 * @Time 2023/12/13 21:01
 */
public interface IndexService{

    /**
     * 首页-商机转换龙虎榜
     * @param indexStatisticsVo
     * @return
     */
    List<Map<String,Object>> businessChangeStatistics(IndexStatisticsVo indexStatisticsVo);

    /**
     * 首页-今日简报
     * @return
     */
    IndexTodayInfoVO getTodayInfo();

    /**
     * 首页-今日代办
     * @return
     */
    IndexTodoInfoVO getTodoInfo(String startTime, String endTime);

    /**
     * 基础数据统计
     * @return
     */
    IndexBaseInfoVO getBaseInfo(String startTime, String endTime);

    /**
     * 首页-线索转换龙虎榜
     * @param indexStatisticsVo
     * @return
     */
    List<Map<String, Object>> salesStatistic(IndexStatisticsVo indexStatisticsVo);
}
