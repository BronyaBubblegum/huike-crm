package com.huike.report.mapper;

import com.huike.report.domain.vo.ReportClueDTO;
import com.huike.report.domain.vo.ReportClueVO;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Description TODO
 * @Author Louis
 * @Time 2023/12/15 14:49
 */
public interface ReportMapper {

    List<ReportClueDTO> cluesStatisticsList(ReportClueDTO reportClueDTO);
}
