package com.huike.report.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.huike.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReportClueDTO  {
    private Long id;
    private String owner;
    private Long deptId;;
    private String post;
    private String beginCreateTime;
    private String endCreateTime;

}
