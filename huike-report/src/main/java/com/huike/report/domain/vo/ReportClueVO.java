package com.huike.report.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReportClueVO {
    private Long id;
    private String name;
    private String phone;
    private String channel;
    private String code;
    private String info;
    private String owner;
    private Long deptId;
    private String deptName;
    private String clueStatus;
    private LocalDateTime clueCreateTime;
    private LocalDateTime businessCreateTime;
    private String businessStatus;
    private LocalDateTime contractCreateTime;
    private String order;
}
