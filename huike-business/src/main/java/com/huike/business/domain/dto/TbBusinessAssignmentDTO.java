package com.huike.business.domain.dto;

import lombok.Data;

import java.util.List;

@Data
public class TbBusinessAssignmentDTO {
    private Long userId;
    private List<Long> ids;
}
