package com.huike.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huike.business.domain.TbBusiness;
import com.huike.business.domain.dto.TbBusinessDTODTO;
import com.huike.clues.domain.vo.IndexStatisticsVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
* @author 93238
* @description 针对表【tb_business(商机)】的数据库操作Mapper
* @createDate 2023-10-12 06:08:37
* @Entity com.huike.business.domain.TbBusiness
*/
public interface TbBusinessMapper extends BaseMapper<TbBusiness> {

    List<TbBusinessDTODTO> pageQuery(TbBusinessDTODTO tbBusinessDTO);

    List<TbBusinessDTODTO> poolList(TbBusinessDTODTO tbBusinessDTO);

    @Update("update tb_business set end_time = #{endTime} where id = #{id}")
    void updateBusinessEndTimeById(Long id, Date endTime);
    @Update("update tb_business set status = #{status} where id = #{id}")
    void setStatus(Long id, String status);

    /**
     * 查询今日商机总数
     * @return
     */
    Integer selectBusinessCount(LocalDateTime startTime, LocalDateTime endTime,String username);

    /**
     * 查询待跟进商机总数
     * @param startTime
     * @param endTime
     * @param username
     * @return
     */
    Integer getToallocatedBusinessNum(LocalDateTime startTime,LocalDateTime endTime, String username);

    /**
     * 查询待分配商机总数
     * @param startTime
     * @param endTime
     * @param username
     * @return
     */
    Integer getTofollowedBusinessNum(LocalDateTime startTime,LocalDateTime endTime, String username);

    /**
     * 查询商机总数
     * @param timeOfStart
     * @param timeOfEnd
     * @param username
     * @return
     */
    Integer selectBusinessCountAll(LocalDateTime timeOfStart, LocalDateTime timeOfEnd, String username);

    int countAllBusiness(String beginCreateTime, String endCreateTime);

    List<Map<String, Object>> countAllContractByUser(@Param("indexVo") IndexStatisticsVo vo);

    Integer getBusinessCount(@Param("begin") LocalDateTime begin, @Param("end") LocalDateTime end);
}




