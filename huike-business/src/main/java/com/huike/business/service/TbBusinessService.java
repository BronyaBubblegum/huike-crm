package com.huike.business.service;

import com.huike.business.domain.TbBusiness;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huike.business.domain.dto.TbBusinessAssignmentDTO;
import com.huike.business.domain.dto.TbBusinessDTODTO;
import com.huike.clues.domain.TbClue;

import java.util.List;

/**
* @author 93238
* @description 针对表【tb_business(商机)】的数据库操作Service
* @createDate 2023-10-12 06:08:37
*/
public interface TbBusinessService extends IService<TbBusiness> {

    List pageQuery(TbBusinessDTODTO tbBusinessDTO);

    List poolList(TbBusinessDTODTO tbBusinessDTO);

    void add(TbBusiness tbBusiness);

    void assignment(TbBusinessAssignmentDTO tbBusiness);

    void gain(TbBusinessAssignmentDTO tbBusiness);

    void changeBusiness(Long id);
}
