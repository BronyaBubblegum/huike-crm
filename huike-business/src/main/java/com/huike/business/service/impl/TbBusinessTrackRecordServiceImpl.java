package com.huike.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huike.business.domain.TbBusinessTrackRecord;
import com.huike.business.domain.vo.BusinessTrackVo;
import com.huike.business.service.TbBusinessTrackRecordService;
import com.huike.business.mapper.TbBusinessTrackRecordMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.util.List;

/**
* @author 93238
* @description 针对表【tb_business_track_record(商机跟进记录)】的数据库操作Service实现
* @createDate 2023-10-12 06:08:37
*/
@Service
public class TbBusinessTrackRecordServiceImpl extends ServiceImpl<TbBusinessTrackRecordMapper, TbBusinessTrackRecord>
    implements TbBusinessTrackRecordService{


    @Resource
    private TbBusinessTrackRecordMapper businessTrackRecordMapper;

    @Override
    public List<TbBusinessTrackRecord> getList(Long businessId) {
        return null;
    }
}




