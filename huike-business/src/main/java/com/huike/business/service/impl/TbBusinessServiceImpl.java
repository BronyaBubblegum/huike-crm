package com.huike.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.LambdaUpdateChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huike.business.domain.TbBusiness;
import com.huike.business.domain.dto.TbBusinessAssignmentDTO;
import com.huike.business.domain.dto.TbBusinessDTODTO;
import com.huike.business.service.TbBusinessService;
import com.huike.business.mapper.TbBusinessMapper;
import com.huike.clues.domain.SysUser;
import com.huike.clues.domain.TbAssignRecord;
import com.huike.clues.domain.TbClue;
import com.huike.clues.mapper.SysUserMapper;
import com.huike.clues.mapper.TbClueMapper;
import com.huike.clues.service.TbAssignRecordService;
import com.huike.clues.service.TbClueService;
import com.huike.clues.service.impl.SysUserServiceImpl;
import com.huike.clues.utils.HuiKeCrmDateUtils;
import com.huike.common.constant.Constants;
import com.huike.common.utils.DateUtils;
import com.huike.common.utils.SecurityUtils;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author 93238
 * @description 针对表【tb_business(商机)】的数据库操作Service实现
 * @createDate 2023-10-12 06:08:37
 */
@Service
public class TbBusinessServiceImpl extends ServiceImpl<TbBusinessMapper, TbBusiness>
        implements TbBusinessService {

    @Autowired
    private TbBusinessMapper tbBusinessMapper;

    @Autowired
    private TbAssignRecordService tbAssignRecordService;

    @Autowired
    private TbClueService tbClueService;

    @Autowired
    private SysUserServiceImpl sysUserService;
    @Resource
    private TbClueMapper clueMapper;

    @Override
    public List pageQuery(TbBusinessDTODTO tbBusinessDTO) {
        return tbBusinessMapper.pageQuery(tbBusinessDTO);
    }

    @Override
    public List poolList(TbBusinessDTODTO tbBusinessDTO) {
        return tbBusinessMapper.poolList(tbBusinessDTO);
    }

    @Override
    public void add(TbBusiness tbBusiness) {
        tbBusiness.setCreateTime(DateUtils.getNowDate());
        tbBusiness.setCreateBy(SecurityUtils.getUsername());
        save(tbBusiness);

        TbAssignRecord record = new TbAssignRecord();
        record.setAssignId(tbBusiness.getId());
        record.setUserId(SecurityUtils.getUserId());
        record.setUserName(SecurityUtils.getUsername());
        record.setDeptId(SecurityUtils.getDeptId());
        record.setCreateBy(SecurityUtils.getUsername());
        record.setCreateTime(DateUtils.getNowDate());
        record.setType(Constants.rule_type_business);
        tbAssignRecordService.save(record);
        // 修改结束时间
        Date endTime = HuiKeCrmDateUtils.getEndDateByRule(record);
        tbBusinessMapper.updateBusinessEndTimeById(tbBusiness.getId(), endTime);
    }

    /**
     * 分配商机
     * @param tbBusiness
     */
    @Override
    public void assignment(TbBusinessAssignmentDTO tbBusiness) {
        TbAssignRecord record = new TbAssignRecord();
        SysUser user = sysUserService.getById(tbBusiness.getUserId());
        record.setUserId(user.getUserId());
        record.setUserName(user.getUserName());
        record.setDeptId(user.getDeptId());
        record.setCreateTime(new Date(System.currentTimeMillis()));
        record.setCreateBy(SecurityUtils.getUsername());
        record.setType("1");
        tbBusiness.getIds().forEach(id -> {
            record.setAssignId(id);
            tbAssignRecordService.saveOrUpdate(record, new QueryWrapper<TbAssignRecord>().eq("assign_id", id));
            // 修改结束时间
            Date endTime = HuiKeCrmDateUtils.getEndDateByRule(record);
            tbBusinessMapper.updateBusinessEndTimeById(id, endTime);
        });
    }


    @Override
    public void gain(TbBusinessAssignmentDTO tbBusiness) {

        TbAssignRecord record = new TbAssignRecord();
        SysUser user = sysUserService.getById(tbBusiness.getUserId());
        record.setUserId(user.getUserId());
        record.setUserName(user.getUserName());
        record.setDeptId(user.getDeptId());
        record.setCreateTime(new Date(System.currentTimeMillis()));
        record.setType("1");
        tbBusiness.getIds().forEach(id -> {
            QueryWrapper<TbAssignRecord> wrapper = new QueryWrapper<>();
            wrapper.eq("assign_id", id);
            TbAssignRecord tbAssignRecord = tbAssignRecordService.getOne(wrapper);
            if (tbAssignRecord != null) {
                record.setId(tbAssignRecord.getId());
                tbAssignRecordService.updateById(record);
            } else {
                record.setCreateBy(SecurityUtils.getUsername());
                record.setAssignId(id);
                tbAssignRecordService.save(record);
            }
            // 修改结束时间
            Date endTime = HuiKeCrmDateUtils.getEndDateByRule(record);
            tbBusinessMapper.setStatus(id, "2");
            tbBusinessMapper.updateBusinessEndTimeById(id, endTime);
        });
    }

    @Override
    public void changeBusiness(Long id) {
        TbClue clue = clueMapper.selectById(id);
        TbBusiness business = new TbBusiness();
        BeanUtils.copyProperties(clue, business);
        business.setCreateTime(DateUtils.getNowDate());
        business.setCreateBy(SecurityUtils.getUsername());
        tbBusinessMapper.insert(business);
        clueMapper.deleteById(id);
    }
}




