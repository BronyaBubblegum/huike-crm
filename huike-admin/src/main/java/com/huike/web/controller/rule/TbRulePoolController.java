package com.huike.web.controller.rule;

import com.huike.clues.domain.dto.TbRulePoolDTO;
import com.huike.clues.service.TbRulePoolService;
import com.huike.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Description 线索或商机池规则相关接口
 * @Date 2023-10-10
 */
@RestController
@RequestMapping("/rule/pool")
public class TbRulePoolController {

    @Autowired
    private TbRulePoolService tbRulePoolService;

    /**
     * 编辑池
     * @param tbRulePoolDTO
     * @return
     */
    @PutMapping
    public ResponseResult editPool(@RequestBody TbRulePoolDTO tbRulePoolDTO){

        return tbRulePoolService.editPool(tbRulePoolDTO);
    }
    /**
     *更具type得到
     */
    @GetMapping("/{type}")
    public ResponseResult getType(@PathVariable("type") String type){

        return tbRulePoolService.getType(type);
    }

}
