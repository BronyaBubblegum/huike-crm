package com.huike.web.controller.system;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.huike.clues.domain.SysNotice;
import com.huike.clues.service.ISysNoticeService;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 通知公告相关接口
 */
@RestController
@RequestMapping("/system/notice")
public class SysNoticeController {

    @Autowired
    private ISysNoticeService noticeService;
    /**
     * 获取系统提醒下拉列表
     */
    @GetMapping("/list/{status}")
    public AjaxResult getNoticeList(@PathVariable("status") String status) {
        return AjaxResult.success(noticeService.selectNoticeList(status));
    }

    /**
     * 获取系统提醒分页列表
     * @param status 状态
     * @return
     */
    @GetMapping("/pagelist/{status}")
    public TableDataInfo getNoticePageList(@PathVariable("status") String status) {
        return noticeService.selectNoticeList(status);
    }

    /**
     * 根据id查询
     * @param noticeId
     * @return
     */
    @GetMapping("/{noticeId}")
    public AjaxResult getNotice(@PathVariable("noticeId") Integer noticeId) {
        return AjaxResult.success(noticeService.getById(noticeId));
    }

    /**
     * 修改未读为已读
     * @param noticeId
     * @return
     */
    @PutMapping("/{noticeId}")
    public AjaxResult setNoticeStatus(@PathVariable("noticeId") Integer noticeId) {
        UpdateWrapper<SysNotice> wrapper = new UpdateWrapper<SysNotice>().eq("notice_id", noticeId).set("status", 1);
        return AjaxResult.success(noticeService.update(wrapper));
    }
}
