package com.huike.web.controller.report;

import com.huike.clues.domain.vo.IndexStatisticsVo;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.report.domain.vo.IndexBaseInfoVO;
import com.huike.report.domain.vo.IndexTodayInfoVO;
import com.huike.report.domain.vo.IndexTodoInfoVO;
import com.huike.report.result.ReportIndexStatisticsData;
import com.huike.report.service.IndexService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Description 首页报表相关接口
 * @Date 2023-10-10
 */
@RestController
@Api(tags = "首页报表相关接口")
@RequestMapping("/index")
public class IndexController extends BaseController {

    @Resource
    private IndexService indexService;

    @ApiOperation("首页-商机转换龙虎榜")
    @GetMapping("/businessChangeStatistics")
    public AjaxResult<List<ReportIndexStatisticsData>> businessChangeStatistics(IndexStatisticsVo indexStatisticsVo) {
        indexStatisticsVo.setBeginCreateTime(indexStatisticsVo.getBeginCreateTime()+" 00:00:00");
        indexStatisticsVo.setEndCreateTime(indexStatisticsVo.getEndCreateTime()+" 23:59:59");
        List<Map<String, Object>> result = indexService.businessChangeStatistics(indexStatisticsVo);
        return AjaxResult.success(result);
    }

    @ApiOperation("首页-线索转换龙虎榜")
    @GetMapping("/salesStatistic")
    public AjaxResult<List<ReportIndexStatisticsData>> salesStatistic(IndexStatisticsVo indexStatisticsVo) {
        indexStatisticsVo.setBeginCreateTime(indexStatisticsVo.getBeginCreateTime()+" 00:00:00");
        indexStatisticsVo.setEndCreateTime(indexStatisticsVo.getEndCreateTime()+" 23:59:59");
        List<Map<String, Object>> result = indexService.salesStatistic(indexStatisticsVo);
        return AjaxResult.success(result);
    }
    @ApiOperation("获取今日简报数据")
    @GetMapping("/getTodayInfo")
    public AjaxResult<IndexTodayInfoVO> getTodayInfo() {
        IndexTodayInfoVO result = indexService.getTodayInfo();
        return AjaxResult.success(result);
    }

    @ApiOperation("获取今日待办数据")
    @GetMapping("/getTodoInfo")
    public AjaxResult<IndexTodoInfoVO> getTodoInfo(@RequestParam("beginCreateTime") String startTime,
                                                   @RequestParam("endCreateTime") String endTime) {
        IndexTodoInfoVO result = indexService.getTodoInfo(startTime, endTime);
        return AjaxResult.success(result);
    }

    @ApiOperation("基础数据统计")
    @GetMapping("/getBaseInfo")
    public AjaxResult<IndexBaseInfoVO> getBaseInfo(@RequestParam("beginCreateTime") String startTime,
                                                   @RequestParam("endCreateTime") String endTime) {
        IndexBaseInfoVO result = indexService.getBaseInfo(startTime,endTime);
        return AjaxResult.success(result);
    }
}
