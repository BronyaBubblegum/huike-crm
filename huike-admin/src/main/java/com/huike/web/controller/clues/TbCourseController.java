package com.huike.web.controller.clues;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.huike.clues.domain.SysUser;
import com.huike.clues.domain.TbCourse;
import com.huike.clues.domain.dto.TbCourseDTO;
import com.huike.clues.service.TbCourseService;
import com.huike.common.constant.HttpStatus;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.entity.SysUserDTO;
import com.huike.common.core.page.PageDomain;
import com.huike.common.core.page.TableDataInfo;
import com.huike.common.core.page.TableSupport;
import com.huike.common.dtos.ResponseResult;
import com.huike.common.utils.StringUtils;
import com.huike.common.utils.sql.SqlUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 课程管理Controller
 * @date 2023-04-01
 */
@RestController
@RequestMapping("/clues/course")
public class TbCourseController extends BaseController {

    @Autowired
    private TbCourseService courseService;

    /**
     * 新增课程
     * @param tbCourse
     * @return
     */
    @PostMapping
    public ResponseResult addCourse(@RequestBody TbCourse tbCourse){
        return courseService.addCourse(tbCourse);
    }

    /**
     * 修改课程
     * @param tbCourse
     * @return
     */
    @PutMapping
    public ResponseResult editCourse(@RequestBody TbCourse tbCourse){
        return courseService.editCourse(tbCourse);
    }

    /**
     * 得到课程列表
     * @param dto
     * @return
     */
    @GetMapping("/list")
    public TableDataInfo getCourseList( TbCourseDTO dto){
        startPage();
        List<TbCourse> lists = courseService.selectCourseList(dto);
        // List<TbCourse> resList=new ArrayList<>();
        // for (TbCourse list : lists) {
        //     Date createTime = list.getCreateTime();
        //     SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        //     String createTimeStr = sdf1.format(createTime);
        // }
        return getDataTable(lists);
    }

    /**
     * 下拉
     * @param subject
     * @return
     */
    @GetMapping("/listselect")
    public ResponseResult listSelect( String subject){
        return courseService.listSelect(subject);
    }

    /**
     * 删除
     * @param ids
     * @return
     */
     @DeleteMapping("/{ids}")
    public ResponseResult delCourse(@PathVariable("ids") String ids){
        return courseService.delCourse(ids);
}

    @GetMapping("/{id}")
    public ResponseResult getCourseId(@PathVariable("id") Long id){
         return courseService.getCourseById(Math.toIntExact(id));
    }


}
