package com.huike.web.controller.report;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.huike.clues.domain.TbActivity;
import com.huike.clues.domain.TbClue;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.page.TableDataInfo;
import com.huike.contract.domain.TbContract;
import com.huike.report.domain.vo.*;
import com.huike.report.result.ReportActivityStatisticsData;
import com.huike.report.result.ReportChannelStatisticsData;
import com.huike.report.result.ReportSubjectStatisticsData;
import com.huike.service.ReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import nonapi.io.github.classgraph.json.JSONUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * @Description 统计分析相关接口
 * @Date 2023-10-10
 */

@Api("统计分析相关接口")
@RestController
@Slf4j
@RequestMapping("/report")
public class ReportController extends BaseController {
    @Autowired
    private ReportService reportService;

    @GetMapping("/contractStatistics/{beginCreateTime}/{endCreateTime}")
    public LineChartVo contractStatistics(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate beginCreateTime,
                                          @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endCreateTime) {
        log.info("新增客户数统计起始时间为:{} 结束时间为:{}", beginCreateTime, endCreateTime);
        return reportService.contractStatistics(beginCreateTime, endCreateTime);
    }

    @GetMapping("/contractStatisticsList")
    public TableDataInfo<List<TbContract>> contractStatisticsList(Integer pageNum,
                                                                  Integer pageSize,
                                                                  @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate beginCreateTime,
                                                                  @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endCreateTime,
                                                                  String channel,
                                                                  String createBy,
                                                                  String deptId
    ) {
        log.info("pageNum:{},pageSize:{},beginCreateTime:{},endCreateTime:{},channel:{},createBy:{},deptId:{}", pageNum, pageSize, beginCreateTime, endCreateTime, channel, createBy, deptId);

        Page<TbContract> tbContractList = reportService.contractReportList(pageNum, pageSize, beginCreateTime, endCreateTime, channel, createBy, deptId);
        return TableDataInfo.success(tbContractList.getResult(), (int) tbContractList.getTotal());
    }

    @GetMapping("/subjectStatistics/{beginCreateTime}/{endCreateTime}")
    public AjaxResult<List<ReportSubjectStatisticsData>> subjectStatistics(@PathVariable String beginCreateTime, @PathVariable String endCreateTime) {
        log.info("新增客户数学科统计起始时间为:{} 结束时间为:{}", beginCreateTime, endCreateTime);
        return AjaxResult.success(reportService.subjectStatistics(beginCreateTime, endCreateTime));
    }

    @GetMapping("/salesStatistics/{beginCreateTime}/{endCreateTime}")
    public LineChartVo salesStatistics(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate beginCreateTime,
                                       @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endCreateTime) {
        log.info("销售统计表起始时间为:{} 结束时间为:{}", beginCreateTime, endCreateTime);
        return reportService.salesStatistics(beginCreateTime, endCreateTime);
    }

    @GetMapping("/deptStatisticsList/{beginCreateTime}/{endCreateTime}")
    public TableDataInfo<List<Map<String, Object>>> deptStatisticsList(Integer pageNum,
                                                                       Integer pageSize,
                                                                       @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate beginCreateTime,
                                                                       @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endCreateTime) {
        log.info("部门统计表起始时间为:{} 结束时间为:{}", beginCreateTime, endCreateTime);
        Page<Map<String, Object>> deptStatisticsList = reportService.deptStatisticsList(pageNum, pageSize, beginCreateTime, endCreateTime);
        return TableDataInfo.success(deptStatisticsList, (int) deptStatisticsList.getTotal());
    }

    @GetMapping("/channelStatisticsList/{beginCreateTime}/{endCreateTime}")
    public TableDataInfo channelStatisticsList(Integer pageNum,
                                               Integer pageSize,
                                               @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate beginCreateTime,
                                               @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endCreateTime) {
        log.info("销售统计归属渠道明细统计表起始时间为:{} 结束时间为:{}", beginCreateTime, endCreateTime);
        startPage();
        List<Map<String, Object>> list = reportService.channelStatisticsList(beginCreateTime, endCreateTime);
        return getDataTable(list);
    }

    @GetMapping("/ownerShipStatisticsList/{beginCreateTime}/{endCreateTime}")
    public TableDataInfo ownerShipStatisticsList(Integer pageNum,
                                                 Integer pageSize,
                                                 @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate beginCreateTime,
                                                 @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endCreateTime) {
        log.info("销售统计归属人报表起始时间为:{} 结束时间为:{}", beginCreateTime, endCreateTime);
        startPage();
        List<Map<String, Object>> list = reportService.ownerShipStatistics(beginCreateTime, endCreateTime);
        return getDataTable(list);
    }

    @GetMapping("/cluesStatistics/{beginCreateTime}/{endCreateTime}")
    public LineChartVo cluesStatistics(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate beginCreateTime,
                                       @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endCreateTime) {
        log.info("线索统计表起始时间为:{} 结束时间为:{}", beginCreateTime, endCreateTime);
        return reportService.cluesStatistics(beginCreateTime, endCreateTime);
    }

    @GetMapping("/cluesStatisticsList")
    public TableDataInfo cluesStatisticsList(ReportClueDTO reportClueVO) {
        startPage();
        List list = reportService.cluesStatisticsList(reportClueVO);
        return getDataTable(list);
    }

    @GetMapping("/getVulnerabilityMap/{beginCreateTime}/{endCreateTime}")
    public AjaxResult<VulnerabilityMapVo> getVulnerabilityMap(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate beginCreateTime,
                                                              @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endCreateTime) {
        log.info("线索转化率表起始时间为:{} 结束时间为:{}", beginCreateTime, endCreateTime);
        VulnerabilityMapVo vulnerabilityMap = reportService.getVulnerabilityMap(beginCreateTime, endCreateTime);
        return AjaxResult.success(vulnerabilityMap);
    }

    @GetMapping("/chanelStatistics/{beginCreateTime}/{endCreateTime}")
    public AjaxResult<List<ReportChannelStatisticsData>> chanelStatistics(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate beginCreateTime,
                                                                          @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endCreateTime) {
        log.info("渠道统计表起始时间为:{} 结束时间为:{}", beginCreateTime, endCreateTime);
        List<ReportChannelStatisticsData> list = reportService.chanelStatistics(beginCreateTime, endCreateTime);
        return AjaxResult.success(list);
    }

    @GetMapping("/activityStatistics/{beginCreateTime}/{endCreateTime}")
    public AjaxResult<List<ReportActivityStatisticsData>> activityStatistics(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate beginCreateTime,
                                                                             @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endCreateTime) {
        log.info("活动统计表起始时间为:{} 结束时间为:{}", beginCreateTime, endCreateTime);
        List<ReportActivityStatisticsData> list = reportService.activityStatistics(beginCreateTime, endCreateTime);
        return AjaxResult.success(list);
    }

    @ApiOperation("渠道统计活动明细列表")
    @GetMapping("/activityStatisticsList")
    public TableDataInfo<List<ActivityStatisticsVo>> activityStatisticsList(TbActivity activity) {
        List<ActivityStatisticsVo> list= reportService.activityStatisticsList(activity);
        return getDataTablePage(list);
    }
    // @GetMapping("/activityStatisticsList")
    // public TableDataInfo activityStatisticsList(Integer pageNum,
    //                                             Integer pageSize,
    //                                             @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate beginCreateTime,
    //                                             @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endCreateTime,
    //
    // ) {
    //     log.info("活动统计表起始时间为:{} 结束时间为:{}", beginCreateTime, endCreateTime);
    //     startPage();
    //     List<ReportActivityStatisticsData> list = reportService.activityStatisticsList(beginCreateTime, endCreateTime);
    // }
}
