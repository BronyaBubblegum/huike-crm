package com.huike.web.controller.contract;

import com.huike.business.mapper.TbBusinessMapper;
import com.huike.clues.domain.SysUser;
import com.huike.clues.mapper.TbClueMapper;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.domain.entity.SysUserDTO;
import com.huike.common.core.page.TableDataInfo;
import com.huike.contract.domain.vo.TransferVo;
import com.huike.contract.result.TransferAssignmentData;
import com.huike.contract.service.TbTransferService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description 转派相关请求
 * @Date 2023-10-10
 */
@RestController
@Api(tags = "转派相关接口")
@RequestMapping("/transfer")
public class TransferController extends BaseController {

    @Resource
    TbTransferService transferService ;

    @GetMapping("/list")
    @ApiOperation("获取转派列表")
    @PreAuthorize("@ss.hasPermi('transfer:transfer:list')")
    public TableDataInfo transferList(SysUserDTO sysUserDTO){
        List<TransferVo> voList  = transferService.gettransferList(sysUserDTO);

        return getDataTablePage(voList);
    }

    @PutMapping("/assignment/{type}/{userId}/{transferUserId}")
    @ApiOperation("转派")
    @PreAuthorize("@ss.hasPermi('transfer:transfer:assignment')")
    public AjaxResult<TransferAssignmentData> assignment(@PathVariable String type,
                                                         @PathVariable Long userId,
                                                         @PathVariable Long transferUserId){

        return AjaxResult.success(transferService.assignment(type,userId,transferUserId));
    }
}
