package com.huike.web.controller.system;

import com.huike.clues.domain.SysDict;
import com.huike.clues.domain.SysDictData;
import com.huike.clues.domain.dto.SysDictDTO;
import com.huike.clues.service.ISysDictService;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.domain.entity.SysDictDataDTO;
import com.huike.common.core.page.TableDataInfo;
import com.huike.common.utils.poi.ExcelUtil;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author a1857
 * @Description 数据字典数据信息接口
 * @Date 2023-10-10
 */

@RestController
@RequestMapping("/system/dict/data")
public class SysDictDataController extends BaseController {

    @Resource
    private ISysDictService sysDictService;
    @PostMapping
    public AjaxResult saveDict(@RequestBody SysDictDTO sysDictDTO){
        sysDictService.saveData(sysDictDTO);
        return AjaxResult.success();
    }

    @GetMapping("/{dictCode}")
    public AjaxResult<SysDict> getDict(@PathVariable Integer dictCode){
        SysDict dict = sysDictService.getDict(dictCode);
        return AjaxResult.success(dict);
    }

    @GetMapping("/type/{dictType}")
    public AjaxResult<List<SysDict>> getDictByType(@PathVariable String dictType){
        return AjaxResult.success(sysDictService.getDictByType(dictType));
    }

    @GetMapping("/list")
    @PreAuthorize("@ss.hasPermi('system:user:list')")
    public TableDataInfo getList(String dictType){
        startPage();
        List<SysDictDataDTO> list = sysDictService.getList(dictType);
        return getDataTable(list);

    }

    @PutMapping
    public AjaxResult updateDict(@RequestBody SysDictDTO sysDictDTO){
        sysDictService.updateDict(sysDictDTO);
        return AjaxResult.success();
    }
    @DeleteMapping("/{dictCodes}")
    public AjaxResult deleteByCode(@PathVariable List<Integer> dictCodes){
        sysDictService.deleteByCode(dictCodes);
        return AjaxResult.success();
    }

    @GetMapping("/export")
    public AjaxResult<String > export(SysDictData listData){
        List<SysDictData> list = sysDictService.selectList(listData);
        ExcelUtil<SysDictData> util = new ExcelUtil<>(SysDictData.class);
        return util.exportExcel(list, "字典数据");
    }
}
