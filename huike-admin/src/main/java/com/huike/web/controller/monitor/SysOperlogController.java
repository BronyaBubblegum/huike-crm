package com.huike.web.controller.monitor;

import com.huike.clues.domain.SysOperLog;
import com.huike.clues.domain.dto.SysOperLogDTO;
import com.huike.clues.domain.dto.TbActivityPageQueryDTO;
import com.huike.clues.service.SysOperlogService;
import com.huike.common.annotation.Log;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.page.TableDataInfo;
import com.huike.common.enums.BusinessType;
import com.huike.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 操作日志记录
 *
 *
 */
@RestController
@RequestMapping("/monitor/operlog")
public class SysOperlogController extends BaseController {

    @Autowired
    private SysOperlogService operlogService;
    /**
     * 分页查询
     */
    @GetMapping("/list")
    @PreAuthorize("@ss.hasPermi('monitor:operlog:list')")
    public TableDataInfo<List<SysOperLog>> pageQuery(SysOperLogDTO operLog,String keyWord) {
        startPage();
        operLog.setTitle(keyWord);
        List<SysOperLogDTO> list =operlogService.pageQuery(operLog);
        return getDataTable(list);
    }

    /**
     * 批量删除
     * @param operIds
     * @return
     */
    @PreAuthorize("@ss.hasPermi('monitor:operlog:remove')")
    @DeleteMapping("/{operIds}")
    public AjaxResult remove(@PathVariable List<Long> operIds) {
        return toAjax(operlogService.removeByIds(operIds));
    }

    /**
     * 清空操作日志记录
     * @return
     */
    @PreAuthorize("@ss.hasPermi('monitor:operlog:remove')")
    @DeleteMapping("/clean")
    public AjaxResult clean() {
        return toAjax(operlogService.clean());
    }

    /**
     * 导出操作日志记录
     * @param operLog
     * @return
     */
    @GetMapping("/export")
    @Log(title = "操作日志", businessType = BusinessType.EXPORT)
    public AjaxResult<String> getOperLog(SysOperLogDTO operLog){
        List<SysOperLogDTO> logList = operlogService.pageQuery(operLog);
        ExcelUtil<SysOperLogDTO> excelUtil = new ExcelUtil<SysOperLogDTO>(SysOperLogDTO.class);
        return excelUtil.exportExcel(logList, "操作日志记录");
    }
}
