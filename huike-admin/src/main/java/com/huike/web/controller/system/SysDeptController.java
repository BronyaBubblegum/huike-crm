package com.huike.web.controller.system;

import com.huike.clues.service.ISysDeptService;
import com.huike.common.constant.UserConstants;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.domain.entity.SysDeptDTO;
import com.huike.common.core.domain.model.SysDept;
import com.huike.common.utils.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.huike.common.utils.SecurityUtils.getUsername;

/**
 * @Description 部门管理相关接口
 * @Date 2023-10-10
 */
@RestController
@RequestMapping("/system/dept")
public class SysDeptController {

    @Resource
    private ISysDeptService deptService;
    /**
     * 部门搜索
     */
    @GetMapping("/list")
    public AjaxResult list(SysDept dept) {
        List<SysDept> depts = deptService.selectDeptList(dept);
        return AjaxResult.success(depts);
    }

    /**
     * 新增部门
     * @param dept
     * @return
     */
    @PostMapping
    public AjaxResult saveDept(@RequestBody SysDept dept){
        //判断出现的部门名字是否存在
        if (UserConstants.NOT_UNIQUE.equals(deptService.checkDeptNameUnique(dept))) {
            return AjaxResult.error("新增部门'" + dept.getDeptName() + "'失败，部门名称已存在");
        }
        dept.setCreateBy(getUsername());
        return AjaxResult.success(deptService.insertDept(dept));
    }

    /**
     *查询部门列表
     * @param deptId
     * @return
     */
    @GetMapping("/list/exclude/{deptId}")
    public AjaxResult excludeDept(@PathVariable("deptId") Integer deptId){
        List<SysDept> depts = deptService.selectDeptList(new SysDept());
        Iterator<SysDept> it = depts.iterator();
        while (it.hasNext())
        {
            SysDept d = (SysDept) it.next();
            if (d.getDeptId().intValue() == deptId
                    || ArrayUtils.contains(StringUtils.split(d.getAncestors(), ","), deptId + ""))
            {
                it.remove();
            }
        }
        return AjaxResult.success(depts);
    }

    /**
     * 根据部门id查找详细信息
     * @param deptId
     * @return
     */
    @GetMapping("/{deptId}")
    public AjaxResult getDeptById(@PathVariable Integer deptId){
        //判断当前用户是否有资格是否在查早范围
        deptService.checkDeptDataScope(Long.valueOf(deptId));
        SysDept sysDept = deptService.selectDeptById(Long.valueOf(deptId));
        return AjaxResult.success(sysDept);
    }

    /**
     * 修改部门****
     *
     * 这个逻辑复杂
     * @return
     */
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody SysDept dept)
    {
        Long deptId = dept.getDeptId();
        deptService.checkDeptDataScope(deptId);

        if (UserConstants.NOT_UNIQUE.equals(deptService.checkDeptNameUnique(dept)))
        {
            return AjaxResult.error("修改部门'" + dept.getDeptName() + "'失败，部门名称已存在");
        }
        else if (dept.getParentId().equals(deptId))
        {
            return AjaxResult.error("修改部门'" + dept.getDeptName() + "'失败，上级部门不能是自己");
        }
        else if (StringUtils.equals(UserConstants.DEPT_DISABLE, dept.getStatus()) && deptService.selectNormalChildrenDeptById(deptId) > 0)
        {
            return AjaxResult.error("该部门包含未停用的子部门！");
        }
        dept.setUpdateBy(getUsername());
        return AjaxResult.success(deptService.updateDept(dept));
    }

    /**
     * 获取对应角色部门列表树
     * @param roleId
     * @return
     */
    @GetMapping("/roleDeptTreeselect/{roleId}")
    public AjaxResult getroleDeptTreeselect(@PathVariable("roleId") Integer roleId) {
        List<SysDept> depts = deptService.selectDeptList(new SysDept());

        Map<String,Object> map=new HashMap();
        List<Long> checkedKeys = deptService.selectDeptListByRoleId(Long.valueOf(roleId));
        map.put("checkedKeys",checkedKeys);
        map.put("depts", deptService.buildDeptTreeSelect(depts));

        return AjaxResult.success(map);
    }
    @DeleteMapping("/{deptId}")
    public AjaxResult delDept(@PathVariable("deptId") Integer deptId) {
        if (deptService.hasChildByDeptId(Long.valueOf(deptId)))
        {
            return AjaxResult.error("存在下级部门,不允许删除");
        }
        if (deptService.checkDeptExistUser(Long.valueOf(deptId)))
        {
            return AjaxResult.error("部门存在用户,不允许删除");
        }
        deptService.checkDeptDataScope(Long.valueOf(deptId));
        return AjaxResult.success(deptService.deleteDeptById(Long.valueOf(deptId)));
    }

    /**
     * 得到拉取树
     * @return
     */
    @GetMapping("/treeselect")
    public AjaxResult getTreeselect(SysDept dept){
        List<SysDept> sysDepts = deptService.selectDeptList(dept);

        //创造select树
        return AjaxResult.success(deptService.buildDeptTreeSelect(sysDepts));
    }

    /**
     * 拿到人员
     * @return
     */
    @GetMapping("/treeAnduser")
    public AjaxResult getTreeAnduser(){
      return AjaxResult.success(deptService.buildDeptAndUserTreeSelect());
    }


}
