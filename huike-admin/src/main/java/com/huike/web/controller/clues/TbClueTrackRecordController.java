package com.huike.web.controller.clues;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.TbClueTrackRecord;
import com.huike.clues.domain.dto.TbClueDTO;
import com.huike.clues.domain.vo.ClueTrackRecordVo;
import com.huike.clues.service.TbClueService;
import com.huike.clues.service.TbClueTrackRecordService;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.page.TableDataInfo;
import com.huike.common.utils.SecurityUtils;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 线索跟进记录Controller
 * @date 2023-04-22
 */
@RestController
@Slf4j
@RequestMapping("/clues/record")
@Api("线索跟进记录管理相关接口")
public class TbClueTrackRecordController extends BaseController {
    @Autowired
    private TbClueTrackRecordService tbClueTrackRecordService;
    @Autowired
    private TbClueService tbClueService;
    /**
     * 根据id获取线索跟进记录
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public AjaxResult getById(@PathVariable Integer id){
        log.info("获取id为{}的线索跟进详细信息",id);
        return AjaxResult.success(tbClueTrackRecordService.getOne(new QueryWrapper<TbClueTrackRecord>().eq("id",id).eq("latest",1)));
    }

    /**
     * 根据id获取线索跟进记录列表
     * @param clueId
     * @return
     */
    @GetMapping("list")
    public TableDataInfo list(Integer clueId){
        log.info("获取id为{}的线索跟进记录列表",clueId);
        startPage();
        List<TbClueTrackRecord> tbClueTrackRecords = tbClueTrackRecordService.getList(clueId);
        return getDataTable(tbClueTrackRecords);
    }

    /**
     * 新增线索跟进记录
     * @param clueTrackRecordVo
     * @return
     */
    @PostMapping
    public AjaxResult addRecord(@RequestBody ClueTrackRecordVo clueTrackRecordVo){
        TbClue tbClue = new TbClue();
        BeanUtils.copyProperties(clueTrackRecordVo,tbClue);
        tbClue.setId(clueTrackRecordVo.getClueId());
        tbClueService.updateById(tbClue);

        TbClueTrackRecord tbClueTrackRecord = new TbClueTrackRecord();
        BeanUtils.copyProperties(clueTrackRecordVo,tbClueTrackRecord);
        tbClueTrackRecord.setClueId(clueTrackRecordVo.getClueId().toString());
        tbClueTrackRecord.setCreateBy(SecurityUtils.getUsername());
        if(tbClueTrackRecord.getCreateTime() == null){
            tbClueTrackRecord.setCreateTime(new java.util.Date());
        }
        return AjaxResult.success(tbClueTrackRecordService.save(tbClueTrackRecord));
    }
}
