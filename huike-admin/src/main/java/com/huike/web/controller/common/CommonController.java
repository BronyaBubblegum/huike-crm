package com.huike.web.controller.common;

import com.huike.common.core.domain.UploadFileAjaxResult;
import com.huike.common.utils.file.AliOssUtil;
import com.huike.common.utils.uuid.UUID;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import com.huike.common.config.HuiKeConfig;
import com.huike.common.utils.StringUtils;
import com.huike.common.utils.file.FileUtils;
import org.springframework.http.MediaType;
import javax.servlet.http.HttpServletRequest;

/**
 * @Description 通用请求处理
 * @Date 2023-10-10
 */
@RestController
@RequestMapping("/common")
@Slf4j
public class CommonController {
    @Autowired
    private AliOssUtil aliOssUtil;

    /**
     * 文件上传
     * @param file
     * @return
     */

    @PostMapping("/upload")
    public UploadFileAjaxResult upload(MultipartFile file){

        log.info("文件上传:{}", file);

        try{
            //原始文件名
            String originalFilename = file.getOriginalFilename();
            String extension = originalFilename.substring(originalFilename.lastIndexOf("."));
            //构造新文件名称
            String newName = UUID.randomUUID() + extension;
            //文件请求路径
            String filePath = aliOssUtil.upload(file.getBytes(), newName);
            return UploadFileAjaxResult.success(filePath);
        }catch (IOException e){
            log.error("文件上传失败: {}",e);
        }
        return UploadFileAjaxResult.error("文件上传失败!");
    }
    /**
     * 通用下载请求
     *
     * @param fileName 文件名称
     * @param delete 是否删除
     */
    @ApiOperation("通用下载请求")
    @GetMapping("/download")
    public void fileDownload(String fileName, Boolean delete, HttpServletResponse response, HttpServletRequest request)
    {
        try
        {
            if (!FileUtils.checkAllowDownload(fileName))
            {
                throw new Exception(StringUtils.format("文件名称({})非法，不允许下载。 ", fileName));
            }
            String realFileName = System.currentTimeMillis() + fileName.substring(fileName.indexOf("_") + 1);
            String filePath = HuiKeConfig.getDownloadPath() + fileName;
            log.info("下载路径 {}", filePath);
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            FileUtils.setAttachmentResponseHeader(response, realFileName);
            FileUtils.writeBytes(filePath, response.getOutputStream());
            if (delete)
            {
                FileUtils.deleteFile(filePath);
            }
        }
        catch (Exception e)
        {
            log.error("下载文件失败", e);
        }
    }
}
