package com.huike.web.controller.business;

import com.huike.business.domain.TbBusiness;
import com.huike.business.domain.vo.BusinessTrackVo;
import com.huike.business.service.TbBusinessService;
import com.huike.business.service.TbBusinessTrackRecordService;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.utils.DateUtils;
import com.huike.common.utils.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.huike.business.domain.TbBusinessTrackRecord;
import com.huike.business.domain.dto.TbBusinessTrackRecordDTO;
import com.huike.business.service.TbBusinessTrackRecordService;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 商机跟进记录Controller
 * @date 2023-04-28
 */
@RestController
@RequestMapping("/business/record")
public class TbBusinessTrackRecordController extends BaseController {
    @Autowired
    private TbBusinessTrackRecordService tbBusinessTrackRecordService;

    @Autowired
    private TbBusinessService tbBusinessService;
    /**
     * 查询商机跟进记录列表
     * @param businessId
     * @return
     */
    @GetMapping("/list")
    public AjaxResult list(Long businessId){
        return AjaxResult.success(tbBusinessTrackRecordService.list(new QueryWrapper<TbBusinessTrackRecord>().eq("business_id",businessId)));
    }

    /**
     * 获取商机跟进记录详细信息
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public AjaxResult get(@PathVariable("id") Long id){
        return AjaxResult.success(tbBusinessTrackRecordService.getById(id));
    }

    /**
     * 新增商机跟进记录
     * @param businessTrackVo
     * @return
     */
    @PostMapping
    public AjaxResult save(@RequestBody BusinessTrackVo businessTrackVo){
        //获取参数中跟进记录相关信息
        TbBusinessTrackRecord tbBusinessTrackRecord = new TbBusinessTrackRecord();
        BeanUtils.copyProperties(businessTrackVo,tbBusinessTrackRecord);
        tbBusinessTrackRecord.setCreateBy(SecurityUtils.getUsername());
        tbBusinessTrackRecord.setCreateTime(DateUtils.getNowDate());
        tbBusinessTrackRecord.setBusinessId(businessTrackVo.getBusinessId().toString());
        tbBusinessTrackRecordService.save(tbBusinessTrackRecord);
        //获取商机相关信息
        TbBusiness tbBusiness = new TbBusiness();
        BeanUtils.copyProperties(businessTrackVo,tbBusiness);
        tbBusiness.setId(businessTrackVo.getBusinessId());
        tbBusinessService.updateById(tbBusiness);
        return AjaxResult.success();
    }

}
