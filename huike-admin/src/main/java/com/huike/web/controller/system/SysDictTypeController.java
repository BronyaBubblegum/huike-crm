package com.huike.web.controller.system;

import com.huike.clues.domain.SysDictType;
import com.huike.clues.service.ISysDictTypeService;
import com.huike.common.annotation.Log;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.domain.entity.SysDictTypeDTO;
import com.huike.common.core.page.TableDataInfo;
import com.huike.common.enums.BusinessType;
import com.huike.common.utils.poi.ExcelUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 个人信息 业务处理
 *
 *
 */
@RestController
@RequestMapping("/system/dict/type")
public class SysDictTypeController extends BaseController {
    @Resource
    ISysDictTypeService dictTypeService;

    @ApiOperation("新增数据字典类型")
    @PostMapping
    public AjaxResult insertDictType(@RequestBody SysDictTypeDTO sysDictTypeDTO){
        dictTypeService.insertDictType(sysDictTypeDTO);
        return AjaxResult.success();
    }

    @ApiOperation("修改数据字典类型")
    @PutMapping
    public AjaxResult updateDictType(@RequestBody SysDictTypeDTO sysDictTypeDTO){
        dictTypeService.updateDictType(sysDictTypeDTO);
        return AjaxResult.success();
    }

    @ApiOperation("清空缓存")
    @DeleteMapping("/clearCache")
    public AjaxResult clearCache(){

        dictTypeService.clearCache();
        return AjaxResult.success();
    }

    @ApiOperation("分页查询字典类型")
    @GetMapping("/list")
    @PreAuthorize("@ss.hasPermi('system:dict:type:list')")
    public TableDataInfo list(SysDictType sysDictType){
        startPage();
        List<SysDictType> list = dictTypeService.selectDictTypeList(sysDictType);
        return getDataTable(list);
    }

    @ApiOperation("查询字典类型详情")
    @GetMapping("{dictId}")
    public AjaxResult getInfo(@PathVariable Long dictId){
        SysDictType sysDictType = dictTypeService.selectDictTypeById(dictId);
        return AjaxResult.success(sysDictType);
    }

    @ApiOperation("获取字典选择框列表")
    @GetMapping("/optionselect")
    public AjaxResult optionselect(){
        List<SysDictType> dictTypes = dictTypeService.optionselect();
        return AjaxResult.success(dictTypes);
    }

    @ApiOperation("导出数据")
    @GetMapping("/export")
    @Log(title = "字典类型", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('system:dict:export')")
    public AjaxResult export(SysDictType sysDictType){
        List<SysDictType> list = dictTypeService.selectDictTypeList(sysDictType);
        ExcelUtil<SysDictType> util = new ExcelUtil<>(SysDictType.class);
        return util.exportExcel(list, "字典类型");
    }

    @ApiOperation("批量删除字典类型")
    @DeleteMapping("/{dictIds}")
    public AjaxResult removeBatchesByIds(@PathVariable List<Long> dictIds){
        dictTypeService.removeBatchesByIds(dictIds);
        return AjaxResult.success();
    }
}
