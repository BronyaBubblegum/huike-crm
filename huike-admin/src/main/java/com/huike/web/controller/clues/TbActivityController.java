package com.huike.web.controller.clues;

import com.huike.clues.domain.TbActivity;
import com.huike.clues.domain.dto.TbActivityDTO;
import com.huike.clues.domain.dto.TbActivityPageQueryDTO;
import com.huike.clues.result.TableDataInfoActivityList;
import com.huike.clues.service.TbActivityService;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.page.PageDomain;
import com.huike.common.core.page.TableDataInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resources;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 活动管理Controller
 * @date 2023-04-01
 */
@RestController
@RequestMapping("/clues/activity")
public class TbActivityController extends BaseController {
    @Autowired
    private TbActivityService tbActivityService;

    /**
     * 添加活动
     */
    @PreAuthorize("@ss.hasPermi('clues:activity:add')")
    @PostMapping
    public AjaxResult addActivity(@RequestBody  TbActivity tbActivity){
        tbActivityService.addActivity(tbActivity);
        return AjaxResult.success();
    }

    /**
     * 修改活动
     */
    @PreAuthorize("@ss.hasPermi('clues:activity:edit')")
    @PutMapping
    public AjaxResult updateActivity(@RequestBody TbActivity tbActivity){
        tbActivityService.updateActivity(tbActivity);
        return AjaxResult.success();
    }
    /**
     * 查询活动管理列表
     */
    @PreAuthorize("@ss.hasPermi('clues:activity:list')")
    @GetMapping("/list")
    public TableDataInfo getActivityList(TbActivity tbActivity){
        startPage();
        List<TbActivity> list = tbActivityService.getActivityList(tbActivity);
        return getDataTable(list);
    }
    /**
     * 获取状态为为2的渠道活动列表
     */
    @GetMapping("/listselect/{channel}")
    public AjaxResult getActivityListByStatus(@PathVariable("channel") String channel){
        return AjaxResult.success(tbActivityService.getActivityListByStatus(channel));
    }

    /**
     * 删除活动管理
     * @param ids 活动id集合
     */
    @PreAuthorize("@ss.hasPermi('clues:activity:remove')")
    @DeleteMapping("/{ids}")
    public AjaxResult deleteByIds(@PathVariable("ids") List<Long> ids){
        tbActivityService.deleteByIds(ids);
        return AjaxResult.success();
    }
    /**
     * 获取活动管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('clues:activity:query')")
    @GetMapping("/{id}")
    public AjaxResult getClueActivityById(@PathVariable("id") String id){
        TbActivity tbActivity =tbActivityService.getClueActivityById(id);
        return AjaxResult.success(tbActivity);
    }
}
