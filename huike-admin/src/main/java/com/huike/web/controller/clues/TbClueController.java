package com.huike.web.controller.clues;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huike.business.service.TbBusinessService;
import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.dto.TbClueDTO;
import com.huike.clues.domain.vo.AssignmentVo;
import com.huike.clues.domain.vo.FalseClueVo;
import com.huike.clues.result.ImportClueData;
import com.huike.clues.domain.vo.TbClueExcelVo;
import com.huike.clues.result.ImportClueData;
import com.huike.clues.service.TbClueService;
import com.huike.clues.utils.easyExcel.ExcelListener;
import com.huike.common.annotation.Log;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.page.TableDataInfo;
import com.huike.common.enums.BusinessType;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * 线索管理Controller
 *
 * @author a1857
 * @date 2023-04-02
 */

@RestController
@Slf4j
@RequestMapping("/clues/clue")
public class TbClueController extends BaseController {
    @Autowired
    private TbClueService clueService;
    @Resource
    private TbBusinessService businessService;

    @GetMapping("/list")
    @PreAuthorize("@ss.hasPermi('system:user:list')")
    public TableDataInfo<List<TbClue>> list(TbClue clue){
        List<TbClue> list = clueService.selectList(clue);
        return getDataTablePage(list);
    }

    @PostMapping
    public AjaxResult add(@RequestBody TbClueDTO tbClueDTO){
        clueService.add(tbClueDTO);
        return AjaxResult.success();
    }

    @PutMapping
    public AjaxResult update(@RequestBody TbClueDTO tbClueDTO){
        clueService.updateClue(tbClueDTO);
        return AjaxResult.success();
    }


    @GetMapping("/pool")
    @PreAuthorize("@ss.hasPermi('system:user:getCluePool')")
    public TableDataInfo<List<TbClue>> getCluePool(TbClue clue){
        startPage();
        List<TbClue> pools = clueService.getPools(clue);
        return getDataTable(pools);
    }

    @GetMapping("/{id}")
    public AjaxResult<TbClue> getById(@PathVariable Long id){
        TbClue clue = clueService.getByClueId(id);
        return AjaxResult.success(clue);
    }

    @ApiOperation("批量捞取")
    @PutMapping("/gain")
    public AjaxResult gain(@RequestBody AssignmentVo assignmentVo){
        clueService.ListGain(assignmentVo);
        return AjaxResult.success();
    }

    @ApiOperation("批量分配")
    @PutMapping("/assignment")
    public AjaxResult assignment(@RequestBody AssignmentVo assignmentVo){
        clueService.assignment(assignmentVo);
        return AjaxResult.success();
    }
    @PutMapping("/false/{id}")
    public AjaxResult falseClue(@PathVariable Long id ,@RequestBody FalseClueVo clueVo){
        clueService.falseClue(id, clueVo);
        return AjaxResult.success();
    }

    @ApiOperation("上传线索")
    @Log(title = "上传线索", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult<ImportClueData> importData(MultipartFile file) throws Exception {
        ExcelListener excelListener = new ExcelListener(clueService);
        EasyExcel.read(file.getInputStream(), TbClueExcelVo.class, excelListener).sheet().doRead();
        return AjaxResult.success(excelListener.getResultData());
    }

    @ApiOperation("线索转商机")
    @PutMapping("/changeBusiness/{id}")
    public AjaxResult changeBusiness(@PathVariable Long id){
        businessService.changeBusiness(id);
        return AjaxResult.success();
    }


}
