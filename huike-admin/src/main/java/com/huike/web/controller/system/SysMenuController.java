package com.huike.web.controller.system;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.R;
import com.huike.clues.domain.SysMenu;
import com.huike.clues.result.RoleMenuTreeAjaxResult;
import com.huike.clues.service.ISysMenuService;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.domain.entity.SysMenuDTO;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@Slf4j
@RequestMapping("/system/menu")
@Api("菜单信息相关接口")
public class SysMenuController {
    @Autowired
    private ISysMenuService sysMenuService;

    /**
     * 新增菜单
     * @param sysMenuDTO
     * @return
     */
    @PostMapping
    public AjaxResult addMenu(@RequestBody SysMenuDTO sysMenuDTO){
        log.info("新增菜单的请求参数为{}",sysMenuDTO);
        sysMenuService.addMenu(sysMenuDTO);
        return AjaxResult.success();
    }

    /**
     * 获取菜单列表
     * @param sysMenuDTO
     * @return
     */
    @GetMapping("/list")
    public AjaxResult list(SysMenuDTO sysMenuDTO){
        log.info("获取菜单列表的请求参数:{}",sysMenuDTO);
        List<SysMenu> list = sysMenuService.getList(sysMenuDTO);
        return AjaxResult.success(list);
    }

    /**
     * 获取角色菜单树
     * @param roleId
     * @return
     */
    @GetMapping("/roleMenuTreeselect/{roleId}")
    public AjaxResult roleMenuTreeselect(@PathVariable Long roleId){
        log.info("获取角色菜单树的角色id:{}",roleId);
        List<SysMenuDTO> list = sysMenuService.selectMenuTreeByUserId(roleId);
        return AjaxResult.success(list);
    }

    /**
     * 获取菜单信息
     * @param menuId
     * @return
     */
    @GetMapping("/{menuId}")
    public  AjaxResult getMenuByMenuId(@PathVariable Long menuId){
        log.info("获取菜单信息的菜单id:{}",menuId);
        SysMenu sysMenu = sysMenuService.getMenuById(menuId);
        return AjaxResult.success(sysMenu);
    }

    /**
     * 删除菜单
     * @param menuId
     * @return
     */
    @DeleteMapping("/{menuId}")
    public AjaxResult deleteByMenuId(@PathVariable Long menuId){
        log.info("删除菜单的请求参数:{}",menuId);
        if(menuId.intValue() == 1){
            return AjaxResult.error("系统菜单，不能删除");
        }
        if(sysMenuService.count(new QueryWrapper<SysMenu>().eq("parent_id",menuId)) > 0){
            return AjaxResult.error("存在子菜单，不允许删除");
        }
        sysMenuService.removeById(menuId);
        return AjaxResult.success();
    }

    /**
     * 更新菜单信息
     * @param sysMenuDTO
     * @return
     */
    @PutMapping
    public AjaxResult updateMenu(@RequestBody SysMenuDTO sysMenuDTO){
        log.info("新的菜单信息为:{}",sysMenuDTO);
        sysMenuService.updateMenu(sysMenuDTO);
        return AjaxResult.success();
    }

    /**
     * 菜单选择树
     * @return
     */
    @GetMapping("/treeselect")
    public AjaxResult treeselect(){
        List<SysMenu> list = sysMenuService.list();
        return AjaxResult.success(list);
    }
}
