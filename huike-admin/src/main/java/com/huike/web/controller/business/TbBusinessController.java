package com.huike.web.controller.business;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.huike.business.domain.TbBusiness;
import com.huike.business.domain.dto.TbBusinessAssignmentDTO;
import com.huike.business.domain.dto.TbBusinessDTODTO;
import com.huike.business.domain.vo.BusinessTrackVo;
import com.huike.business.service.TbBusinessService;
import com.huike.clues.domain.TbClue;
import com.huike.clues.service.TbClueService;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商机Controller
 * @date 2023-04-25
 */
@RestController
@RequestMapping("/business")
public class TbBusinessController extends BaseController {
    @Autowired
    private TbBusinessService tbBusinessService;

    @Autowired
    private TbClueService tbClueService;

    /**
     * 商机分页查询
     * @param tbBusinessDTO
     * @return
     */
    @GetMapping("/list")
    @PreAuthorize("@ss.hasPermi('business:business:list')")
    public TableDataInfo list(TbBusinessDTODTO tbBusinessDTO) {
        startPage();
        List list=tbBusinessService.pageQuery(tbBusinessDTO);
        return getDataTable(list);
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @PreAuthorize("@ss.hasPermi('business:business:query')")
    public AjaxResult get(@PathVariable("id") Long id){
        TbBusinessDTODTO tbBusinessDTODTO = new TbBusinessDTODTO();
        tbBusinessDTODTO.setId(id);
        return AjaxResult.success(tbBusinessService.pageQuery(tbBusinessDTODTO).get(0));
    }

    /**
     * 公海池
     * @return
     */
    @GetMapping("/pool")
    @PreAuthorize("@ss.hasPermi('business:business:pool')")
    public TableDataInfo pool(TbBusinessDTODTO tbBusinessDTO){
        startPage();
        return getDataTable(tbBusinessService.poolList(tbBusinessDTO));
    }

    /**
     * 新增商机
     * @param tbBusiness
     * @return
     */
    @PostMapping()
    @PreAuthorize("@ss.hasPermi('business:business:add')")
    public AjaxResult save(@RequestBody TbBusiness tbBusiness){
        if (tbClueService.getOne(new QueryWrapper<TbClue>().eq("phone", tbBusiness.getPhone()))!=null) {
            return AjaxResult.error("手机号已存在");
        }
        tbBusinessService.add(tbBusiness);
        return AjaxResult.success();
    }

    /**
     * 修改商机
     * @param tbBusiness
     * @return
     */
    @PutMapping()
    @PreAuthorize("@ss.hasPermi('business:business:edit')")
    public AjaxResult update(@RequestBody TbBusiness tbBusiness){
        tbBusinessService.updateById(tbBusiness);
        return AjaxResult.success();
    }

    /**
     * 分配商机
     * @param tbBusiness
     * @return
     */
    @PutMapping("/assignment")
    @PreAuthorize("@ss.hasPermi('business:business:assignment')")
    public AjaxResult assignment(@RequestBody TbBusinessAssignmentDTO tbBusiness){
        tbBusinessService.assignment(tbBusiness);
        return AjaxResult.success();
    }

    /**
     * 踢回公海
     * @param id
     * @param reason
     * @return
     */
    @PutMapping("/back/{id}/{reason}")
    @PreAuthorize("@ss.hasPermi('business:business:back')")
    public AjaxResult back(@PathVariable("id") Long id,@PathVariable("reason") String reason){
        UpdateWrapper<TbBusiness> wrapper = new UpdateWrapper<TbBusiness>().eq("id", id).set("status", "1");
        tbBusinessService.update(wrapper);
        return AjaxResult.success();
    }

    /**
     * 批量捞取
     * @param tbBusiness
     * @return
     */
    @PutMapping("/gain")
    @PreAuthorize("@ss.hasPermi('business:business:gain')")
    public AjaxResult gain(@RequestBody TbBusinessAssignmentDTO tbBusiness){
        tbBusinessService.gain(tbBusiness);
        return AjaxResult.success();
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @DeleteMapping("/{ids}")
    @PreAuthorize("@ss.hasPermi('business:business:remove')")
    public AjaxResult delete(@PathVariable("ids") List<Long> ids){
        tbBusinessService.removeByIds(ids);
        return AjaxResult.success();
    }

}
