package com.huike.web.controller.system;

import com.huike.clues.domain.SysRole;
import com.huike.clues.service.ISysRoleService;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.domain.entity.SysRoleDTO;
import com.huike.common.core.domain.entity.SysUserDTO;
import com.huike.common.core.page.TableDataInfo;
import com.huike.common.dtos.ResponseResult;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.github.pagehelper.page.PageMethod.startPage;

@RequestMapping("/system/role")
@RestController
public class SysRoleController extends BaseController {

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }


 @Resource
 private ISysRoleService sysRoleService;

    /**
     * 新增角色
     * @param dto
     * @return
     */
  @PostMapping
    public ResponseResult saveRole(@RequestBody SysRoleDTO dto){

       return sysRoleService.saveRole(dto);
  }

  @DeleteMapping("/{roleIds}")
  public ResponseResult DeletByIds(@PathVariable("roleIds") String roleIds){
      return sysRoleService.DeletByIds(roleIds);
  }


    /**
     * 修改角色
     * @param dto
     * @return
     */
  @PutMapping
  public AjaxResult editRole(@Validated @RequestBody SysRoleDTO dto){
       sysRoleService.editRole(dto);
       return AjaxResult.success();
  }

    /**
     * 根据数据范围
     * @param dto
     * @return
     */
  @PutMapping("/dataScope")
  public ResponseResult editDataScope(@RequestBody SysRoleDTO dto){
      return sysRoleService.editDataScope(dto);
  }
  @PutMapping("/changeStatus")
  public ResponseResult editStatus(@RequestBody SysRoleDTO dto){
      return sysRoleService.editStatus(dto);
  }

    /**
     * 分页查询
     * @param role
     * @return
     */
    @GetMapping("/list")
    public TableDataInfo<SysRole> listRole(SysRoleDTO role){
        startPage();
        List<SysRoleDTO> list = sysRoleService.selectUserList(role);
        return getDataTable(list);
    }

    /**
     *根据id查询详细信息
     */
    @GetMapping("/{roleId}")
    public ResponseResult getRoleBtId(@PathVariable("roleId") Integer roleId){
        return sysRoleService.getRoleBtId(roleId);
    }




}