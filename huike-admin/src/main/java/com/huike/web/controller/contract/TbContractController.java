package com.huike.web.controller.contract;

import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.page.TableDataInfo;
import com.huike.contract.domain.TbContract;
import com.huike.contract.domain.dto.TbContractDTO;
import com.huike.contract.domain.dto.TbContractPageDTO;
import com.huike.contract.service.TbContractService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 合同Controller
 */
@RestController
@RequestMapping("/contract")
@Api(tags ="合同管理相关接口")
public class TbContractController extends BaseController {

    @Autowired
    private TbContractService contractService;

    @PostMapping
    @ApiOperation("新增合同")
    @PreAuthorize("@ss.hasPermi('system:dict:type:list')")
    public AjaxResult insertContract(@RequestBody TbContractDTO tbContractDTO){
        contractService.insertContract(tbContractDTO);

        return AjaxResult.success();
    }

    @ApiOperation("商机转合同")
    @PutMapping("/changeContract/{id}")
    public AjaxResult changeContract(@PathVariable Long id,@RequestBody TbContractDTO tbContractDTO){
        contractService.changeContract(id,tbContractDTO);
        return AjaxResult.success();
    }
    @ApiOperation("查询合同列表")
    @GetMapping("/list")
    public TableDataInfo list(TbContractPageDTO tbContractPageDTO){
        startPage();
        List<TbContract> contractList = contractService.getContractList(tbContractPageDTO);
        return getDataTable(contractList);
    }
    @GetMapping("/detail/{id}")
    @ApiOperation("获取合同信息1")
    public AjaxResult getContractDetailOne(@PathVariable Long id){
        TbContract tbContract = contractService.selectContractDetailOneAndTwo(id);

        return AjaxResult.success(tbContract);
    }


    @ApiOperation("获取合同信息2")
    @GetMapping("/{id}")
    public AjaxResult getContractDetailTwo(@PathVariable Long id){
        TbContract tbContract = contractService.selectContractDetailOneAndTwo(id);

        return AjaxResult.success(tbContract);
    }
}
