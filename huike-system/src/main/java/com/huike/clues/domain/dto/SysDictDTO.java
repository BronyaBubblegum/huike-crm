package com.huike.clues.domain.dto;

import lombok.Data;

/**
 * @author a1857
 */
@Data
public class SysDictDTO {
    private Integer dictCode;
    private String dictLabel;
    private String dictSort;
    private String dictType;
    private String dictValue;
    private String remark;
    private String status;
}
