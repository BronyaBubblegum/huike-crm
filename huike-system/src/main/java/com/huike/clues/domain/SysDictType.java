package com.huike.clues.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.huike.common.annotation.Excel;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @Description TODO
 * @Author Louis
 * @Time 2023/12/13 12:02
 */
@Data
@TableName("sys_dict_type")
public class SysDictType {

    @TableId(value = "dict_id")
    @Excel(name = "字典编码", cellType = Excel.ColumnType.NUMERIC)
    private Long dictId;

    /** 字典名称 */
    @Excel(name = "字典名称")
    private String dictName;

    /** 字典类型 */
    @Excel(name = "字典类型")
    private String dictType;

    /** 状态（0正常 1停用） */
    @TableField("status")
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    private String createBy;

    private String updateBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:dd:ss")
    private LocalDateTime createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:dd:ss")
    private LocalDateTime updateTime;

    @TableField("remark")
    private String remark;

    @JsonFormat(pattern = "yyyy-MM-dd HH:dd:ss")
    @TableField(exist = false)
    private LocalDateTime begin;

    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:dd:ss")
    private LocalDateTime end;
}
