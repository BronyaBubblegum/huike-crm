package com.huike.clues.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huike.clues.domain.SysOperLog;
import com.huike.clues.domain.dto.SysOperLogDTO;
import org.springframework.stereotype.Service;

import java.util.List;

public interface SysOperlogService extends IService<SysOperLog> {
    List<SysOperLogDTO> pageQuery(SysOperLogDTO operLog);

    int clean();
}
