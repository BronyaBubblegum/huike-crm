package com.huike.clues.service;

import java.util.List;
import java.util.Set;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huike.clues.domain.SysRole;
import com.huike.common.core.domain.entity.SysRoleDTO;
import com.huike.common.core.page.TableDataInfo;
import com.huike.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 角色业务层
 *
 *
 */
public interface ISysRoleService extends IService<SysRole>
{
    /**
     * 查询所有角色
     *
     * @return 角色列表
     */
    public List<SysRoleDTO> selectRoleAll();

    /**
     * 根据条件分页查询角色数据
     *
     * @param role 角色信息
     * @return 角色数据集合信息
     */
    public List<SysRoleDTO> selectRoleList(SysRoleDTO role);

    /**
     * 根据用户ID获取角色选择框列表
     *
     * @param userId 用户ID
     * @return 选中角色ID列表
     */
    public List<Integer> selectRoleListByUserId(Long userId);

    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    public Set<String> selectRolePermissionByUserId(Long userId);


    /**
     * 新增角色
     * @param dto
     * @return
     */

    public ResponseResult saveRole(SysRoleDTO dto);


    /**
     * 分页查询
     * @param role
     * @return
     */


    List<SysRoleDTO> selectUserList(SysRoleDTO role);

    /**
     * 根据id查询
     * @param roleId
     * @return
     */
    ResponseResult getRoleBtId(Integer roleId);

    /**
     * 修改角色
     * @param dto
     * @return
     */
    ResponseResult editRole(SysRoleDTO dto);

    /**
     *
     * @param dto
     * @return
     */
    ResponseResult editDataScope(SysRoleDTO dto);

    /**
     * 更改状态
     * @param dto
     * @return
     */
    ResponseResult editStatus(SysRoleDTO dto);

    /**
     * 删除
     * @param roleIds
     * @return
     */
    ResponseResult DeletByIds(String roleIds);
}