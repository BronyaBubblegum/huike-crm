package com.huike.clues.service.impl;

import java.util.*;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huike.clues.domain.SysRole;
import com.huike.clues.domain.dto.SysRoleDeptDTO;
import com.huike.clues.domain.dto.SysRoleMenuDTO;
import com.huike.clues.mapper.SysRoleDeptMapper;
import com.huike.clues.mapper.SysRoleMenuMapper;
import com.huike.common.dtos.ResponseResult;
import com.huike.common.enums.AppHttpCodeEnum;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import com.huike.common.core.domain.entity.SysRoleDTO;
import com.huike.common.utils.StringUtils;
import com.huike.common.utils.spring.SpringUtils;
import com.huike.clues.mapper.SysRoleMapper;
import com.huike.clues.service.ISysRoleService;

import javax.annotation.Resource;

/**
 * 角色 业务层处理
 *
 *
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService
{
    @Resource
    private SysRoleMapper roleMapper;


    /**
     * 查询所有角色
     *
     * @return 角色列表
     */
    @Override
    public List<SysRoleDTO> selectRoleAll()
    {
        return SpringUtils.getAopProxy(this).selectRoleList(new SysRoleDTO());
    }

    /**
     * 根据条件分页查询角色数据
     *
     * @param role 角色信息
     * @return 角色数据集合信息
     */
    @Override
    public List<SysRoleDTO> selectRoleList(SysRoleDTO role)
    {
        return roleMapper.selectRoleList(role);
    }

    /**
     * 根据用户ID获取角色选择框列表
     *
     * @param userId 用户ID
     * @return 选中角色ID列表
     */
    @Override
    public List<Integer> selectRoleListByUserId(Long userId)
    {
        return roleMapper.selectRoleListByUserId(userId);
    }
    /**
     * 根据用户ID查询权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    @Override
    public Set<String> selectRolePermissionByUserId(Long userId)
    {
        List<SysRoleDTO> perms = roleMapper.selectRolePermissionByUserId(userId);
        Set<String> permsSet = new HashSet<>();
        for (SysRoleDTO perm : perms)
        {
            if (StringUtils.isNotNull(perm))
            {
                permsSet.addAll(Arrays.asList(perm.getRoleKey().trim().split(",")));
            }
        }
        return permsSet;
    }



    @Resource
    private SysRoleDeptMapper sysRoleDeptMapper;
    @Resource
    private SysRoleMenuMapper sysRoleMenuMapper;
    /**
     * 新增角色
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult saveRole(SysRoleDTO dto) {

        if (dto==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        SysRole sysRole=new SysRole();
        BeanUtils.copyProperties(dto,sysRole);


        //批量加入角色与部门的关系

        Long[] deptIds = dto.getDeptIds();
        if (deptIds.length!=0){
            List<SysRoleDeptDTO> sysRoleDeptDTOs=new ArrayList<>();
            for (Long deptId : deptIds) {
                SysRoleDeptDTO sysRoleDeptDTO=new SysRoleDeptDTO();
                sysRoleDeptDTO.setRoleId(dto.getRoleId());
                sysRoleDeptDTO.setDeptId(deptId);
                sysRoleDeptDTOs.add(sysRoleDeptDTO);
            }
            sysRoleDeptMapper.batchRoleDept(sysRoleDeptDTOs);
        }


        Long[] menuIds = dto.getMenuIds();
        if (menuIds.length!=0){
            List<SysRoleMenuDTO> sysRoleMenuDTOS=new ArrayList<>();
            for (Long menuId : menuIds) {
                SysRoleMenuDTO sysRoleMenuDTO=new SysRoleMenuDTO();
                sysRoleMenuDTO.setMenuId(menuId);
                sysRoleMenuDTO.setRoleId(dto.getRoleId());
                sysRoleMenuDTOS.add(sysRoleMenuDTO);
            }
            sysRoleMenuMapper.batchRoleMenu(sysRoleMenuDTOS);
        }
        //TODO
        //差个得到当前用户
        sysRole.setRoleSort(Integer.valueOf(dto.getRoleSort()));
        sysRole.setCreateTime(new Date());
        sysRole.setUpdateTime(new Date());
        sysRole.setDelFlag("0");


        save(sysRole);


        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 分页查询
     *
     * @param role
     * @return
     */
    @Override
    public List<SysRoleDTO> selectUserList(SysRoleDTO role) {
        return roleMapper.selectRoleList(role);
    }

    /**
     * 根据id查询
     *
     * @param roleId
     * @return
     */
    @Override
    public ResponseResult getRoleBtId(Integer roleId) {

        SysRole byId = getById(roleId);
        return ResponseResult.okResult(byId);
    }

    /**
     * 修改角色
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult editRole(SysRoleDTO dto){
        if (dto==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }


        SysRole sysRole=new SysRole();
        BeanUtils.copyProperties(dto,sysRole);
        sysRole.setUpdateTime(new Date());
        updateById(sysRole);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * @param dto
     * @return
     *
     * 编辑数据范围
     */
    @Override
    public ResponseResult editDataScope(SysRoleDTO dto) {
        if (dto==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }


        SysRole sysRole=new SysRole();
        BeanUtils.copyProperties(dto,sysRole);
        sysRole.setUpdateTime(new Date());
        updateById(sysRole);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 更改状态
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult editStatus(SysRoleDTO dto) {
        if (dto==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        SysRole sysRole=new SysRole();
        BeanUtils.copyProperties(dto,sysRole);
        sysRole.setUpdateTime(new Date());
        updateById(sysRole);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 删除
     *
     * @param roleIds
     * @return
     */
    @Override
    public ResponseResult DeletByIds(String roleIds) {
        String[] splits = roleIds.split(",");
        Long[] ids=new Long[splits.length];
        int index=0;
        for (String split : splits) {
            ids[index]= Long.valueOf(split);
            index++;
        }

        removeByIds(Arrays.asList(ids));
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

}
