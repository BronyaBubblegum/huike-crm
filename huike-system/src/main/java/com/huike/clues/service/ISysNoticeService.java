package com.huike.clues.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huike.clues.domain.SysNotice;
import com.huike.common.core.page.TableDataInfo;

import java.util.List;

public interface ISysNoticeService extends IService<SysNotice> {
    TableDataInfo selectNoticeList(String status);
}
