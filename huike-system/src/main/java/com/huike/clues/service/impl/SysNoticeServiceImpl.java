package com.huike.clues.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huike.clues.domain.SysNotice;
import com.huike.clues.mapper.SysNoticeMapper;
import com.huike.clues.service.ISysNoticeService;
import com.huike.common.core.page.TableDataInfo;
import com.huike.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysNoticeServiceImpl extends ServiceImpl<SysNoticeMapper, SysNotice> implements ISysNoticeService {
    @Autowired
    private SysNoticeMapper sysNoticeMapper;
    @Override
    public TableDataInfo selectNoticeList(String status) {
        List<SysNotice> list = sysNoticeMapper.selectList(new QueryWrapper<SysNotice>()
                .eq("status", status)
                .eq("notice_user_id", SecurityUtils.getUserId()));
        return TableDataInfo.success(list,list.size());
    }
}
