package com.huike.clues.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huike.clues.domain.SysOperLog;
import com.huike.clues.domain.dto.SysOperLogDTO;
import com.huike.clues.mapper.SysOperlogMapper;
import com.huike.clues.service.SysOperlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysOperlogServiceImpl extends ServiceImpl<SysOperlogMapper, SysOperLog> implements SysOperlogService {
    @Autowired
    private SysOperlogMapper sysOperlogMapper;

    @Override
    public List<SysOperLogDTO> pageQuery(SysOperLogDTO operLog) {
        return sysOperlogMapper.pageQuery(operLog);
    }

    @Override
    public int clean() {
        return sysOperlogMapper.clean();
    }
}
