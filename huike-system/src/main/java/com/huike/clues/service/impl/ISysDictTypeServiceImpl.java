package com.huike.clues.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huike.clues.domain.SysDictType;
import com.huike.clues.mapper.SysDictTypeMapper;
import com.huike.clues.service.ISysDictTypeService;
import com.huike.common.core.domain.entity.SysDictTypeDTO;
import com.huike.common.utils.DictUtils;
import com.huike.common.utils.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Description TODO
 * @Author Louis
 * @Time 2023/12/13 12:01
 */
@Service
public class ISysDictTypeServiceImpl extends ServiceImpl<SysDictTypeMapper, SysDictType> implements ISysDictTypeService {

    @Resource
    private SysDictTypeMapper sysDictTypeMapper;

    @Override
    public void insertDictType(SysDictTypeDTO sysDictTypeDTO) {
        SysDictType sysDictType = new SysDictType();

        BeanUtils.copyProperties(sysDictTypeDTO, sysDictType);
        String username = SecurityUtils.getUsername();
        sysDictType.setCreateBy(username);
        sysDictType.setUpdateBy(username);
        sysDictType.setCreateTime(LocalDateTime.now());
        sysDictType.setUpdateTime(LocalDateTime.now());
        sysDictTypeMapper.insert(sysDictType);
    }

    @Override
    public void updateDictType(SysDictTypeDTO sysDictTypeDTO) {

        SysDictType sysDictType = new SysDictType();

        BeanUtils.copyProperties(sysDictTypeDTO, sysDictType);
        String username = SecurityUtils.getUsername();
        sysDictType.setUpdateBy(username);
        sysDictType.setUpdateTime(LocalDateTime.now());
        sysDictType.setRemark(sysDictTypeDTO.getRemark());
        sysDictTypeMapper.updateById(sysDictType);
    }

    @Override
    public List<SysDictType> selectDictTypeList(SysDictType sysDictType) {

        return lambdaQuery().like(sysDictType.getDictName() != null, SysDictType::getDictName, sysDictType.getDictName())
                .like(sysDictType.getDictType() != null, SysDictType::getDictType, sysDictType.getDictType())
                .eq(sysDictType.getStatus() != null, SysDictType::getStatus, sysDictType.getStatus())
                .between(null != sysDictType.getBegin() && null != sysDictType.getEnd(),
                        SysDictType::getCreateTime, sysDictType.getBegin(), sysDictType.getEnd())
                .list();
    }

    @Override
    public void clearCache() {
        DictUtils.clearDictCache();
    }

    @Override
    public SysDictType selectDictTypeById(Long dictId) {
        return sysDictTypeMapper.selectById(dictId);
    }

    @Override
    public List<SysDictType> optionselect() {
        return sysDictTypeMapper.selectList(new QueryWrapper<>());
    }


    @Override
    public void removeBatchesByIds(List<Long> dictIds) {
        sysDictTypeMapper.deleteBatchIds(dictIds);
    }
}
