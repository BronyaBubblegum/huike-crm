package com.huike.clues.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huike.clues.domain.SysDict;
import com.huike.clues.domain.SysDictData;
import com.huike.clues.domain.dto.SysDictDTO;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.domain.entity.SysDictDataDTO;

import java.util.List;

/**
 * @author a1857
 */
public interface ISysDictService extends IService<SysDict> {

    void saveData(SysDictDTO sysDictDTO);

    void updateDict(SysDictDTO sysDictDTO);

    List<SysDict> getDictByType(String dictType);

    List<SysDictDataDTO> getList(String dictType);

    SysDict getDict(Integer dictCode);

    void deleteByCode(List<Integer> dictCode);


    List<SysDictData> selectList(SysDictData listData);
}
