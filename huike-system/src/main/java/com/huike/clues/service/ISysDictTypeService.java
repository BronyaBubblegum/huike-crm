package com.huike.clues.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huike.clues.domain.SysDictType;
import com.huike.common.core.domain.entity.SysDictTypeDTO;

import java.util.List;

/**
 * 参数配置 服务层
 *
 *
 */

public interface ISysDictTypeService extends IService<SysDictType>
{


    /**
     * 新增字典类型
     * @param sysDictTypeDTO
     */
    void insertDictType(SysDictTypeDTO sysDictTypeDTO);

    /**
     * 修改字典类型
     * @param sysDictTypeDTO
     */
    void updateDictType(SysDictTypeDTO sysDictTypeDTO);

    /**
     * 清空缓存
     */
    void clearCache();

    /**
     * 根据id查询字典类型详情
     * @param dictId
     * @return
     */
    SysDictType selectDictTypeById(Long dictId);

    /**
     * 根据字典类型删除
     * @param dictIds
     */
    void removeBatchesByIds(List<Long> dictIds);

    /**
     * 分页查询字典类型
     * @param sysDictType
     * @return
     */
    List<SysDictType> selectDictTypeList(SysDictType sysDictType);

    /**
     * 获取字典选择框列表
     * @return
     */
    List<SysDictType> optionselect();

}
