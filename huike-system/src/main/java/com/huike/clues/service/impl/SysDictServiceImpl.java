package com.huike.clues.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huike.clues.domain.SysDict;
import com.huike.clues.domain.SysDictData;
import com.huike.clues.domain.dto.SysDictDTO;
import com.huike.clues.mapper.SysDictMapper;
import com.huike.clues.service.ISysDictService;
import com.huike.common.core.domain.entity.SysDictDataDTO;
import com.huike.common.utils.SecurityUtils;
import com.huike.common.utils.bean.BeanUtils;
import io.swagger.models.auth.In;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author a1857
 */

@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements ISysDictService {

    @Resource
    private SysDictMapper sysDictMapper;

    @Override
    public void saveData(SysDictDTO sysDictDTO) {
        SysDict sysDict = new SysDict();
        BeanUtils.copyProperties(sysDictDTO, sysDict);
        sysDict.setCreateTime(LocalDateTime.now());
        sysDict.setCreateBy(SecurityUtils.getUsername());
        sysDictMapper.insert(sysDict);
    }

    @Override
    public void updateDict(SysDictDTO sysDictDTO) {
        SysDict sysDict = new SysDict();
        BeanUtils.copyProperties(sysDictDTO, sysDict);
        sysDict.setUpdateBy(SecurityUtils.getUsername());
        sysDict.setUpdateTime(LocalDateTime.now());
        sysDictMapper.updateByType(sysDict);
    }

    @Override
    public List<SysDict> getDictByType(String dictType) {
        return sysDictMapper.selectByDictType(dictType);
    }

    @Override
    public List<SysDictDataDTO> getList(String dictType) {
        return sysDictMapper.getList(dictType);
    }

    @Override
    public SysDict getDict(Integer dictCode) {
        return  sysDictMapper.selectByCode(dictCode);
    }

    @Override
    public void deleteByCode(List<Integer> dictCodes) {
        sysDictMapper.deleteByCode(dictCodes);
    }

    @Override
    public List<SysDictData> selectList(SysDictData listData) {
        return sysDictMapper.selectDictDataList(listData);
    }

}
