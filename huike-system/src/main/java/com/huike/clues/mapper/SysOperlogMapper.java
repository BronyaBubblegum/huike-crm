package com.huike.clues.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huike.clues.domain.SysOperLog;
import com.huike.clues.domain.dto.SysOperLogDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysOperlogMapper extends BaseMapper<SysOperLog> {
    List<SysOperLogDTO> pageQuery( SysOperLogDTO operLog);

    int clean();
}
