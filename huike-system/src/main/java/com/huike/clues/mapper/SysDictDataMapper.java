package com.huike.clues.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SysDictDataMapper {
    String selectDictLabel(@Param("dictType") String dictType, @Param("dictValue") String dictValue);
}
