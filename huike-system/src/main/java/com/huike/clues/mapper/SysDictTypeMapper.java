package com.huike.clues.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huike.clues.domain.SysDictType;

/**
 * @Description TODO
 * @Author Louis
 * @Time 2023/12/13 12:01
 */
public interface SysDictTypeMapper extends BaseMapper<SysDictType> {
}
