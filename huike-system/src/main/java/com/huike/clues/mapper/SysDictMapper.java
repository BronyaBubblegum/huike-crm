package com.huike.clues.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huike.clues.domain.SysDict;
import com.huike.clues.domain.SysDictData;
import com.huike.common.core.domain.entity.SysDictDataDTO;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author a1857
 */
@Mapper
public interface SysDictMapper extends BaseMapper<SysDict> {

    void add(SysDict sysDict);

    @Select("select * from huike2.sys_dict_data where dict_type = #{dictType}")
    List<SysDict> selectByDictType(String dictType);

    @Select("select * from huike2.sys_dict_data where dict_type = #{dictType}")
    List<SysDictDataDTO> getList(String dictType);


    // void add(SysDict sysDict);

    @Select("select * from huike2.sys_dict_data where dict_code = #{dictCode}")
    SysDict selectByCode(Integer dictCode);

    @Update("update huike2.sys_dict_data set dict_label = #{dictLabel}, dict_sort = #{dictSort}, dict_type = #{dictType}," +
            " dict_value = #{dictValue}, remark = #{remark}, status = #{status} , " +
            "update_time = #{updateTime}, update_by = #{updateBy} where dict_code = #{dictCode}")
    void updateByType(SysDict sysDict);

    void deleteByCode(List<Integer> dictCodes);

    List<SysDictData> selectDictDataList(SysDictData listData);

    @Select("select dict_value from sys_dict_data where dict_type = #{dictType} and dict_label = #{dictLabel}")
    String selectDictValue(String dictType, String channel);
}
