package com.huike.contract.mapper;

import com.huike.clues.domain.TbActivity;
import com.huike.contract.domain.TbContract;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @author 93238
 * @description 针对表【tb_contract(合同)】的数据库操作Mapper
 * @createDate 2023-10-12 06:42:55
 * @Entity com.huike.contract.domain.TbContract
 */
public interface TbContractMapper extends BaseMapper<TbContract> {

    @Select("select count(id) from tb_contract where create_time >= #{beginTime} and create_time <= #{endTime} and status = 1")
    Integer countNewUser(@Param("beginTime") LocalDateTime beginTime, @Param("endTime") LocalDateTime endTime);


    /**
     * 今日合同统计
     * @param start
     * @param end
     * @return
     */
    Integer selectContractCount(LocalDateTime start, LocalDateTime end,String username);

    /**
     * 今日销售额统计
     * @param start
     * @param end
     * @return
     */
    Double selectSalesAmount(LocalDateTime start, LocalDateTime end,String username);

    /**
     * 合同统计
     * @param timeOfStart
     * @param timeOfEnd
     * @param username
     * @return
     */
    Integer selectContractCountAll(LocalDateTime timeOfStart, LocalDateTime timeOfEnd, String username);

    /**
     * 合同销售额统计
     * @param timeOfStart
     * @param timeOfEnd
     * @param username
     * @return
     */
    Double selectSalesAmountAll(LocalDateTime timeOfStart, LocalDateTime timeOfEnd, String username);

    Integer countByMap(Map map);

    List<TbContract> getList(@Param("beginCreateTime") LocalDate beginCreateTime, @Param("endCreateTime") LocalDate endCreateTime, @Param("channel") String channel, @Param("createBy") String createBy, @Param("deptId") String deptId);


    List<Map<String, Object>> subjectStatistics(@Param("beginCreateTime") String beginCreateTime, @Param("endCreateTime") String endCreateTime);

    List<Integer> getSales(@Param("beginTime") LocalDateTime beginTime, @Param("endTime") LocalDateTime endTime);

    List<Map<String, Object>> getDeptStatisticsList(@Param("beginCreateTime") LocalDate beginCreateTime, @Param("endCreateTime") LocalDate endCreateTime);

    List<Map<String, Object>> channelStatisticsList(@Param("beginCreateTime") LocalDate beginCreateTime, @Param("endCreateTime") LocalDate endCreateTime);

    List<Map<String, Object>> ownerShipStatistics(@Param("beginCreateTime") LocalDate beginCreateTime, @Param("endCreateTime") LocalDate endCreateTime);

    Integer countByTime(@Param("begin") LocalDateTime begin, @Param("end") LocalDateTime end);

    List<Map<String, Object>> chanelStatistics(@Param("begin") LocalDateTime begin, @Param("end") LocalDateTime end);

    public List<Map<String,Object>> activityStatistics(@Param("beginCreateTime") String beginCreateTime, @Param("endCreateTime") String endCreateTime);

    Map<String, Object> countByActivity(TbActivity tbContract);
}




