package com.huike.contract.service;

import com.huike.contract.domain.TbContract;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huike.contract.domain.dto.TbContractDTO;
import com.huike.contract.domain.dto.TbContractPageDTO;

import java.util.List;

/**
* @author 93238
* @description 针对表【tb_contract(合同)】的数据库操作Service
* @createDate 2023-10-12 06:42:55
*/
public interface TbContractService extends IService<TbContract> {

    /**
     * 新增合同
     * @param
     */
    void insertContract(TbContractDTO tbContractDTO);

    /**
     * 查询合同列表
     * @param tbContractPageDTO
     * @return
     */
    List<TbContract> getContractList(TbContractPageDTO tbContractPageDTO);

    /**
     * 查询合同详情1
     * @param id
     * @return
     */
    TbContract selectContractDetailOneAndTwo(Long id);


    /**
     * 商机转合同
     * @param id
     */
    void changeContract(Long id, TbContractDTO tbContractDTO);
}
