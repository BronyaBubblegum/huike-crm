package com.huike.contract.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huike.business.mapper.TbBusinessMapper;
import com.huike.clues.domain.SysUser;
import com.huike.clues.domain.TbAssignRecord;
import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.TbRulePool;
import com.huike.clues.mapper.SysUserMapper;
import com.huike.clues.mapper.TbAssignRecordMapper;
import com.huike.clues.mapper.TbClueMapper;
import com.huike.clues.mapper.TbRulePoolMapper;
import com.huike.clues.service.TbRulePoolService;
import com.huike.clues.utils.HuiKeCrmDateUtils;
import com.huike.common.constant.Constants;
import com.huike.common.core.domain.entity.SysUserDTO;
import com.huike.common.utils.DateUtils;
import com.huike.common.utils.bean.BeanUtils;
import com.huike.contract.domain.vo.TransferVo;
import com.huike.contract.service.TbTransferService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @Description TODO
 * @Author Louis
 * @Time 2023/12/14 8:52
 */
@Service
public class TbTransferServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements TbTransferService {

    @Resource
    private TbAssignRecordMapper txAssignRecordMapper;
    @Resource
    private SysUserMapper userMapper;
    @Resource
    private TbRulePoolMapper txRulePoolMapper;
    @Resource
    private TbBusinessMapper tbBusinessMapper;
    @Resource
    private TbClueMapper clueMapper;;

    @Override
    public List<TransferVo> gettransferList(SysUserDTO sysUserDTO) {

        //转派列表数据
        List<TransferVo> transferVoList = new ArrayList<>();
        // 查询出用户列表
        List<SysUserDTO> userList = userMapper.selectUserList(sysUserDTO);
        // 遍历用户集合封装返回数据
        for (SysUserDTO userDTO : userList) {
            // 线索数量
            Integer clueCount = txAssignRecordMapper.selectClueCount(userDTO.getUserId());
            // 商机数量
            Integer businessCount = txAssignRecordMapper.selectBusinessCount(userDTO.getUserId());
            TransferVo transferVo = new TransferVo();
            BeanUtils.copyProperties(userDTO,transferVo);
            transferVo.setBusinessNum(businessCount);
            transferVo.setClueNum(clueCount);
            transferVoList.add(transferVo);
        }
        return transferVoList;
    }

    @Override
    public Map<String,Object> assignment(String type, Long userId, Long transferUserId) {
        Map<String,Object> result = new HashMap<>();
        // 如果当前操作类型为线索
        if(TbAssignRecord.RecordType.CLUES.getValue().equals(type)){
            // 查询出线索数量
            Integer clueCount = txAssignRecordMapper.selectClueCount(userId);
            // 如果不为空
            if (clueCount != null) {
                // 查询出线索池
                TbRulePool tbRulePool = txRulePoolMapper.selectTbRulePoolByType(Constants.rule_type_clue);
                Integer transferUserClueNum = txAssignRecordMapper.selectClueCount(transferUserId);
                // 如果被派送人保有量达到最大值
                if (transferUserClueNum >= tbRulePool.getMaxNunmber()){
                    result.put("flag",false);
                    result.put("msg","线索转换失败！已经达到被转派人最大保有量");
                }else {
                    TbAssignRecord assignRecord = new TbAssignRecord();
                    assignRecord.setUserId(userId);
                    assignRecord.setLatest("1");
                    assignRecord.setType(type);
                    List<TbAssignRecord> list = txAssignRecordMapper.selectAssignRecordList(assignRecord);
                    for (int i = 0; i < list.size(); i++) {
                        TbAssignRecord tbAssignRecord = list.get(i);
                        SysUserDTO user = userMapper.selectUserById(transferUserId);
                        tbAssignRecord.setUserId(userId);
                        tbAssignRecord.setUserName(user.getUserName());
                        tbAssignRecord.setDeptId(user.getDeptId());
                        tbAssignRecord.setCreateTime(DateUtils.getNowDate());
                        txAssignRecordMapper.updateById(tbAssignRecord);
                        clueMapper.setTransfer(tbAssignRecord.getAssignId(), TbClue.StatusType.UNFOLLOWED.getValue());
                        Date endDate = HuiKeCrmDateUtils.getEndDateByRule(tbAssignRecord);
                        clueMapper.updateClueEndTimeById(tbAssignRecord.getAssignId(),endDate);
                        if (transferUserClueNum + i >= tbRulePool.getMaxNunmber()) {
                            result.put("flag",false);
                            result.put("msg","线索转换失败！已经分配" + i + " 线索");
                            break;
                        }
                    }
                }
            }
        }
        if (TbAssignRecord.RecordType.BUSNIESS.getValue().equals(type)) {
            int busniessNum = txAssignRecordMapper.selectBusinessCount(userId);
            if (busniessNum >= 0) {
                TbRulePool rulePool = txRulePoolMapper.selectTbRulePoolByType(Constants.rule_type_business);
                int transferUserBusinessNum = txAssignRecordMapper.selectBusinessCount(transferUserId);
                //被转派人保有量达到最大值
                if (transferUserBusinessNum >= rulePool.getMaxNunmber()) {
                    result.put("flag",false);
                    result.put("msg","线索转换失败！已经达到被转派人最大保有量");
                } else {
                    TbAssignRecord assignRecord = new TbAssignRecord();
                    assignRecord.setUserId(userId);
                    assignRecord.setLatest("1");
                    assignRecord.setType(TbAssignRecord.RecordType.BUSNIESS.getValue());
                    List<TbAssignRecord> list = txAssignRecordMapper.selectAssignRecordList(assignRecord);
                    for (int i = 0; i < list.size(); i++) {
                        TbAssignRecord tbAssignRecord = list.get(i);
                        SysUserDTO sysUser = userMapper.selectUserById(transferUserId);
                        tbAssignRecord.setUserId(transferUserId);
                        tbAssignRecord.setUserName(sysUser.getUserName());
                        tbAssignRecord.setDeptId(sysUser.getDeptId());
                        tbAssignRecord.setCreateTime(DateUtils.getNowDate());
                        txAssignRecordMapper.updateById(tbAssignRecord);
                        Date endDate = HuiKeCrmDateUtils.getEndDateByRule(tbAssignRecord);
                        tbBusinessMapper.updateBusinessEndTimeById(tbAssignRecord.getAssignId(),endDate);
                        if (transferUserBusinessNum + i >= rulePool.getMaxNunmber()) {
                            result.put("flag",false);
                            result.put("msg","商机转换失败！已经转派" + i + " 商机");
                            break;
                        }
                    }
                }
            }
        }
        return result;
    }
}
