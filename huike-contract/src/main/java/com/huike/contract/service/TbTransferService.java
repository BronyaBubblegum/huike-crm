package com.huike.contract.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huike.clues.domain.SysUser;
import com.huike.common.core.domain.entity.SysUserDTO;
import com.huike.contract.domain.vo.TransferVo;

import java.util.List;
import java.util.Map;

/**
 * @Description TODO
 * @Author Louis
 * @Time 2023/12/14 8:49
 */
public interface TbTransferService extends IService<SysUser>{
    /**
     * 转派列表查询
     * @param
     * @return
     */
    List<TransferVo> gettransferList(SysUserDTO sysUserDTO);

    /**
     * 转派
     * @param type
     * @param userId
     * @param transferUserId
     * @return
     */
    Map<String,Object> assignment(String type, Long userId, Long transferUserId);
}
