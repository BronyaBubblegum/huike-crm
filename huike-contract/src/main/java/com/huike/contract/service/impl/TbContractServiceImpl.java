package com.huike.contract.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huike.business.domain.TbBusiness;
import com.huike.business.domain.TbBusinessTrackRecord;
import com.huike.business.domain.dto.TbBusinessDTODTO;
import com.huike.business.domain.dto.TbBusinessTrackRecordDTO;
import com.huike.business.mapper.TbBusinessMapper;
import com.huike.business.mapper.TbBusinessTrackRecordMapper;
import com.huike.clues.domain.TbActivity;
import com.huike.clues.domain.TbCourse;
import com.huike.clues.mapper.TbActivityMapper;
import com.huike.clues.mapper.TbCourseMapper;
import com.huike.common.exception.CustomException;
import com.huike.common.utils.DateUtils;
import com.huike.common.utils.SecurityUtils;
import com.huike.contract.domain.TbContract;
import com.huike.contract.domain.dto.TbContractDTO;
import com.huike.contract.domain.dto.TbContractPageDTO;
import com.huike.contract.mapper.TbContractMapper;
import com.huike.contract.service.TbContractService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @author 93238
 * @description 针对表【tb_contract(合同)】的数据库操作Service实现
 * @createDate 2023-10-12 06:42:55
 */
@Service
public class TbContractServiceImpl extends ServiceImpl<TbContractMapper, TbContract>
        implements TbContractService {

    @Resource
    private TbContractMapper tbContractMapper;
    @Resource
    private TbCourseMapper tbCourseMapper;
    @Resource
    private TbActivityMapper tbActivityMapper;
    @Resource
    private TbBusinessMapper tbBusinessMapper;

    @Override
    public void insertContract(TbContractDTO tbContractDTO) {

        TbContract tbContract = new TbContract();
        BeanUtils.copyProperties(tbContractDTO, tbContract);
        tbContract.setCreateBy(SecurityUtils.getUsername());
        tbContract.setCreateTime(DateUtils.getNowDate());
        tbContract.setCreateBy(SecurityUtils.getUsername());
        tbContract.setCreateTime(DateUtils.getNowDate());
        tbContract.setDeptId(SecurityUtils.getDeptId());
        tbContract.setStatus("4");// 状态(待审核1，已通过2，已驳回3 全部完成4)
        setOrder(tbContract);
        tbContractMapper.insert(tbContract);
    }

    @Override
    public List<TbContract> getContractList(TbContractPageDTO tbContractPageDTO) {

        return lambdaQuery()
                .eq(null != tbContractPageDTO.getContractNo(), TbContract::getContractNo, tbContractPageDTO.getContractNo())
                .like(null != tbContractPageDTO.getName(), TbContract::getName, tbContractPageDTO.getName())
                .like(null != tbContractPageDTO.getPhone(), TbContract::getPhone, tbContractPageDTO.getPhone())
                .eq(null != tbContractPageDTO.getCourseId(), TbContract::getCourseId, tbContractPageDTO.getCourseId())
                .eq(null != tbContractPageDTO.getSubject(), TbContract::getSubject, tbContractPageDTO.getSubject())
                .between(null != tbContractPageDTO.getBegin() && null != tbContractPageDTO.getEnd(),
                        TbContract::getCreateTime, tbContractPageDTO.getBegin(), tbContractPageDTO.getEnd())
                .list();
    }

    @Override
    public void changeContract(Long id, TbContractDTO tbContractDTO) {
        // 查询出商机
        TbBusinessDTODTO tbBusinessDTODTO = new TbBusinessDTODTO();
        tbBusinessDTODTO.setId(id);
        // 得到当前要操作的商机
        TbBusinessDTODTO business = tbBusinessMapper.pageQuery(tbBusinessDTODTO).get(0);
        TbContract tbContract = new TbContract();
        if (business.getCourseId() == null) {
            throw new CustomException("商机里面没有选择课程，无法转换客户合同");
        }
        tbContract.setCreateTime(DateUtils.getNowDate());
        tbContract.setBusinessId(business.getId());
        BeanUtils.copyProperties(business, tbContract);
        insertContract(tbContractDTO);

    }

    @Override
    public TbContract selectContractDetailOneAndTwo(Long id) {
        return tbContractMapper.selectById(id);
    }


    private void setOrder(TbContract tbContract) {
        TbCourse course = tbCourseMapper.selectById(tbContract.getCourseId());
        tbContract.setCoursePrice(Double.valueOf(course.getPrice()));
        TbActivity tbActivity = tbActivityMapper.selectById(tbContract.getActivityId());
        if (tbActivity != null) {
            if ("1".equals(tbActivity.getType())) {
                tbContract.setDiscountType("课程折扣");
                BigDecimal price = new BigDecimal(course.getPrice());
                BigDecimal disCount = new BigDecimal(tbActivity.getDiscount());
                BigDecimal order = price.multiply(disCount.divide(new BigDecimal(10), 2, BigDecimal.ROUND_CEILING));
                tbContract.setContractOrder(Double.valueOf(order.floatValue()));
            } else {
                // 当代金券大于等于合同50% 按照原价
                if (tbActivity.getVouchers() >= course.getPrice() / 2) {
                    tbContract.setContractOrder(Double.valueOf(course.getPrice()));
                    tbContract.setDiscountType("代金券大于原课程50%,代金券不可用");
                } else {
                    tbContract.setDiscountType("代金券");
                    tbContract.setContractOrder(Double.valueOf(course.getPrice() - tbActivity.getVouchers()));
                }
            }
        } else {
            tbContract.setDiscountType("无");
            tbContract.setContractOrder(Double.valueOf(course.getPrice()));
        }
    }
}




