package com.huike.contract.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.huike.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 合同
 * @TableName tb_contract
 */
@TableName(value ="tb_contract")
@Data
public class TbContract  {
    /**
     * 合同id
     */
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 手机号
     */
    @TableField(value = "phone")
    private String phone;

    /**
     * 合同编号
     */
    @TableField(value = "contract_no")
    private String contractNo;

    /**
     * 客户姓名
     */
    @TableField(value = "name")
    private String name;

    /**
     * 意向学科
     */
    @TableField(value = "subject")
    private String subject;

    /**
     * 活动id
     */@TableField(value = "activity_id")
    private Long activityId;

    /**
     * 活动名称
     */
    @TableField(value = "activity_name")
    private String activityName;

    /**
     * 课程id
     */
    @TableField(value = "course_id")
    private Long courseId;

    /**
     * 课程名称
     */
    @TableField(value = "course_name")
    private String courseName;

    /**
     *
     */
    @TableField(value = "channel")
    private String channel;

    /**
     * 状态(待审核1，已通过2，已驳回3 全部完成4)
     */
    @TableField(value = "status")
    private String status;

    /**
     * 创建人
     */
    @TableField(value = "create_by")
    private String createBy;

    /**
     *
     */@TableField(value = "dept_id")
    private Long deptId;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 文件名称
     */
    @TableField(value = "file_name")
    private String fileName;

    /**
     * 订单价格
     */
    @TableField(value = "contract_order")
    private Double contractOrder;

    /**
     * 折扣类型
     */
    @TableField(value = "discount_type")
    private String discountType;

    /**
     * 课程价格
     */
    @TableField(value = "course_price")
    private Double coursePrice;

    /**
     *
     */
    @TableField(value = "process_instance_id")
    private String processInstanceId;

    /**
     *
     */
    @TableField(value = "business_id")
    private Long businessId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
