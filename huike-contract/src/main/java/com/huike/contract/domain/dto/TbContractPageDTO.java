package com.huike.contract.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.huike.common.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @Description TODO
 * @Author Louis
 * @Time 2023/12/13 18:32
 */
@Data
public class TbContractPageDTO {

    /** 合同id */
    @ApiModelProperty("合同id")
    private String id;

    /** 合同编号 */
    @ApiModelProperty("合同编号")
    @Excel(name = "合同编号")
    private String contractNo;

    /** 手机号 */
    @ApiModelProperty("手机号")
    @Excel(name = "手机号")
    private String phone;

    /** 客户姓名 */
    @ApiModelProperty("客户姓名")
    @Excel(name = "客户姓名")
    private String name;

    /** 意向学科 */
    @ApiModelProperty("意向学科")
    @Excel(name = "学科")
    private String subject;

    @ApiModelProperty("渠道")
    private String channel; //渠道

    /** 活动信息 */
    @ApiModelProperty("活动信息")
    @Excel(name = "活动信息")
    private Long activityId;

    /** 课程id */
    @ApiModelProperty("课程id")
    @Excel(name = "课程id")
    private Long courseId;

    /** 状态(待审核0，已完成1，已驳回2) */
    @ApiModelProperty("状态(待审核0，已完成1，已驳回2)")
    @Excel(name = "状态(待审核0，已完成1，已驳回2)")
    private String status;


    /** 文件名称 */
    @ApiModelProperty("文件名称")
    @Excel(name = "文件名称")
    private String fileName;


    //课程价格
    @ApiModelProperty("课程价格")
    private float coursePrice;

    //活动折扣类型
    @ApiModelProperty("活动折扣类型")
    private String discountType;

    @ApiModelProperty("订单价格")
    private float order;

    @ApiModelProperty("完成时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date finishTime;

    @ApiModelProperty("商机id")
    private Long  businessId;

    @ApiModelProperty("部门id")
    private Long deptId;

    @ApiModelProperty("开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime begin;
    @ApiModelProperty("结束时间")
    private LocalDateTime end;
}
