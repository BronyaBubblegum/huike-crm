package com.huike.clues.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.huike.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 线索
 * @TableName tb_clue
 */
@TableName(value ="tb_clue")
@Data
public class TbClue extends BaseEntity {
    /**
     * 线索id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 客户姓名
     */
    private String name;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 渠道
     */
    private String channel;

    /**
     * 活动id
     */
    private Long activityId;

    /**
     * 1 男 0 女
     */
    private String sex;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 微信
     */
    private String weixin;

    /**
     * qq
     */
    private String qq;

    /**
     * 意向等级
     */
    private String level;

    /**
     * 意向学科
     */
    private String subject;

    /**
     * 状态(已分配1  进行中2  回收3  伪线索4)
     */
    private String status;

    /**
     * 创建时间
     */
    // @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    // private LocalDateTime createTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 伪线索失败次数(最大数3次)
     */
    private Integer falseCount;

    /**
     *下次跟进时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date nextTime;

    /**
     *
     */
    // @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    // private LocalDateTime updateTime;

    /**
     * 是否转派
     */
    private String transfer;

    /**
     * 线索失效时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date endTime;

    @TableField(exist = false)
    private String assignBy;
    @TableField(exist = false)
    private String owner;
    @TableField(exist = false)
    private LocalDateTime ownerTime;
    @TableField(exist = false)
    private String activityName;
    @TableField(exist = false)
    private String activityInfo;


    public enum StatusType{


        UNFOLLOWED("待跟进","1"),
        FOLLOWING("跟进中","2"),
        RECOVERY("回收","3"),
        FALSE("伪线索/踢回公海","4"),
        DELETED("删除","5"),
        TOBUSINESS("转换商机","6"),
        TOCUSTOMER("转换客户","7");

        private String name;
        private String value;

        private StatusType(String name,String value)
        {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }
    }


    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    public enum ImportDictType
    {

        CHANNEL("渠道来源","clues_item"),

        SUBJECT("意向学科","course_subject"),

        LEVEL("意向学科","clues_level"),

        SEX("性别","sys_user_sex");


        private String name;
        private String dictType;

        private ImportDictType(String name, String dictType)
        {
            this.name = name;
            this.dictType = dictType;
        }

        public String getName() {
            return name;
        }

        public String getDictType() {
            return dictType;
        }
    }
}
