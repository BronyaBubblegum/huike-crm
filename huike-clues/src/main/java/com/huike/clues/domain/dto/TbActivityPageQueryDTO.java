package com.huike.clues.domain.dto;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 活动管理分页查询 tb_page_query_activity
 *
 * @author lzy
 * @date 2021-04-02
 */
@Data
public class TbActivityPageQueryDTO {
    // id
    private Integer id;
    // 渠道来源
    private String channel;
    // 编码
    private String code;
    // 创建者
    private String createBy;
    // 课程折扣
    private Float discount;
    // 活动简介
    private String info;
    // 活动名称
    private String name;
    //
    private String params;
    // 备注
    private String remark;
    // 搜索值
    private String searchValue;
    // 状态
    private String status;
    // 活动类型
    private String type;
    // 更新者
    private String updateBy;
    // 课程代金券
    private Integer vouchers;
    // 开始时间
    private LocalDateTime beginTime;
    // 创建时间
    private LocalDateTime createTime;
    // 更新时间
    private LocalDateTime updateTime;
    // 结束时间
    private LocalDateTime endTime;
}
