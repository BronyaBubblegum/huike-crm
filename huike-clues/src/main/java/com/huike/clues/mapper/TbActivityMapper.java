package com.huike.clues.mapper;

import com.huike.clues.domain.TbActivity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
* @author 93238
* @description 针对表【tb_activity(活动管理)】的数据库操作Mapper
* @createDate 2023-10-12 06:35:46
* @Entity com.huike.clues.domain.TbActivity
*/
@Mapper
public interface TbActivityMapper extends BaseMapper<TbActivity> {

    @Select("select * from tb_activity")
    List<TbActivity> getActivityList(TbActivity tbActivity);

    void addActivity(TbActivity activity);

    @Select("select * from tb_activity where id=#{id}")
    TbActivity getClueActivityById(String id);

    String selectActivityNameById(@Param("activityId") Integer activityId);
    void updateActivity(TbActivity tbActivity);
    @Select("select * from tb_activity where channel=#{channel}")
    List<TbActivity> getByChannel(String channel);

    @Select("select * from tb_activity where id = #{id}")
    TbActivity getById(Long id);

    TbActivity selectTbActivityByCode(String code);

    List<TbActivity> selectTbActivityList(TbActivity query);
}




