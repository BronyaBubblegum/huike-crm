package com.huike.clues.mapper;

import com.huike.clues.domain.TbRulePool;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 93238
* @description 针对表【tb_rule_pool(线索池规则)】的数据库操作Mapper
* @createDate 2023-10-12 06:35:46
* @Entity com.huike.clues.domain.TbRulePool
*/
public interface TbRulePoolMapper extends BaseMapper<TbRulePool> {

    TbRulePool selectTbRulePoolByType(String ruleTypeClue);
}




