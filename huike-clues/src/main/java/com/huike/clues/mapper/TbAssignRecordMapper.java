package com.huike.clues.mapper;

import com.huike.clues.domain.TbAssignRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;


/**
* @author 93238
* @description 针对表【tb_assign_record(分配记录表)】的数据库操作Mapper
* @createDate 2023-10-12 06:35:46
* @Entity com.huike.clues.domain.TbAssignRecord
*/
public interface TbAssignRecordMapper extends BaseMapper<TbAssignRecord> {

    @Select("select * from huike2.tb_assign_record where assign_id = #{aid} and latest=1")
    TbAssignRecord getById(Long aid);

    void updateByAsgId(Long id);

    List<TbAssignRecord> getByIds(Long[] ids);

    @Select("select * from huike2.tb_assign_record where assign_id = #{id}")
    TbAssignRecord getByAssId(Long id);

    @Update("update huike2.tb_assign_record set user_name = #{userName} where assign_id = #{assignId}")
    void updateByAssId(TbAssignRecord assignRecord);
    /**
     * 查询线索总数
     * @param userId
     * @return
     */
    Integer selectClueCount(Long userId);

    /**
     * 查询商机总数
     * @param userId
     * @return
     */
    Integer selectBusinessCount(Long userId);

    List<TbAssignRecord> selectAssignRecordList(TbAssignRecord assignRecord);
}




