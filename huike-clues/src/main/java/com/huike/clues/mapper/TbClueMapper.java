package com.huike.clues.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huike.clues.domain.TbActivity;
import com.huike.clues.domain.TbClue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.dto.TbClueDTO;
import com.huike.clues.domain.vo.AssignmentVo;
import com.huike.clues.domain.vo.IndexStatisticsVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.Date;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
* @author 93238
* @description 针对表【tb_clue(线索)】的数据库操作Mapper
* @createDate 2023-10-12 06:35:46
* @Entity com.huike.clues.domain.TbClue
*/
public interface TbClueMapper extends BaseMapper<TbClue> {

    List<TbClueDTO> getList(TbClueDTO clue);

    @Select("select * from tb_clue where phone=#{phone}")
    TbClue selectTbClueByPhone(String phone);


    /**
     * 查询今日线索数量
     * @param startTime
     * @param endTime
     * @return
     */
    Integer selectCluesCount(LocalDateTime startTime, LocalDateTime endTime,String username);

    /**
     * 首页--今日待办--待跟进线索数量
     * @param startTime
     * @param endTime
     * @param username
     * @return
     */
    Integer getToallocatedCluesNum(LocalDateTime startTime, LocalDateTime endTime,String username);

    /**
     * 首页--今日待办--待分配线索数量
     * @param startTime
     * @param endTime
     * @param username
     * @return
     */
    Integer getTofollowedCluesNum(LocalDateTime startTime, LocalDateTime endTime, String username);

    Integer selectCluesCountAll(LocalDateTime timeOfStart, LocalDateTime timeOfEnd, String username);


    List<TbClue> getList(TbClue clue);

    void add(TbClue tbClue);


    List<TbClue> getTbClueList(TbClue clue);

    @Select("select * from huike2.tb_clue where id = #{id}")
    TbClue getById(Long id);


    void setTransfer(Long id, String status);

    void updateClueEndTimeById(Long id, Date endDate);

    List<TbClue> getByIds(AssignmentVo assignmentVo);

    int countAllClue(String beginCreateTime, String endCreateTime);

    List<Map<String, Object>> countAllClueByUser(@Param("indexVo")IndexStatisticsVo indexStatisticsVo);

    Integer countClueByTime(@Param("beginTime") LocalDateTime beginTime, @Param("endTime") LocalDateTime endTime);

    Integer countUsefulClueByTime(@Param("begin") LocalDateTime begin, @Param("end") LocalDateTime end);

    Map<String, Object> countByActivity(TbActivity tbClue);
}




