package com.huike.clues.mapper;

import com.huike.clues.domain.TbCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * @author 93238
 * @description 针对表【tb_course(课程管理)】的数据库操作Mapper
 * @createDate 2023-10-12 06:35:46
 * @Entity com.huike.clues.domain.TbCourse
 */
public interface TbCourseMapper extends BaseMapper<TbCourse> {

    String selectCourseNameById(Long courserId);

    List<Map<String, Object>> getClueList(@Param("beginCreateTime") LocalDate beginCreateTime, @Param("endCreateTime") LocalDate endCreateTime, @Param("id") Integer id, @Param("owner") String owner, @Param("post") String post, @Param("deptId") String deptId);
}




