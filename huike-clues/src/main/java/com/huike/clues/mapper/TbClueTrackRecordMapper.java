package com.huike.clues.mapper;

import com.huike.clues.domain.TbClueTrackRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 93238
 * @description 针对表【tb_clue_track_record(线索跟进记录)】的数据库操作Mapper
 * @createDate 2023-10-12 06:35:46
 * @Entity com.huike.clues.domain.TbClueTrackRecord
 */
public interface TbClueTrackRecordMapper extends BaseMapper<TbClueTrackRecord> {

    @Select("select * from tb_clue_track_record where clue_id=#{clueId} ")
    List<TbClueTrackRecord> getList(Integer clueId);


    @Select("SELECT * FROM huike2.tb_clue_track_record ORDER BY create_time DESC LIMIT 1")
    TbClueTrackRecord selectByRecordId(Long id);
}




