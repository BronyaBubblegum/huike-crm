package com.huike.clues.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huike.clues.domain.TbActivity;
import com.huike.clues.domain.TbAssignRecord;
import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.TbClueTrackRecord;
import com.huike.clues.domain.dto.TbClueDTO;
import com.huike.clues.domain.vo.AssignmentVo;
import com.huike.clues.domain.vo.FalseClueVo;
import com.huike.clues.domain.vo.TbClueExcelVo;
import com.huike.clues.mapper.*;
import com.huike.clues.service.TbActivityService;
import com.huike.clues.service.TbAssignRecordService;
import com.huike.clues.service.TbClueService;
import com.huike.clues.mapper.TbClueMapper;
import com.huike.clues.service.TbClueTrackRecordService;
import com.huike.common.annotation.DataScope;
import com.huike.common.exception.CustomException;
import com.huike.common.utils.DateUtils;
import com.huike.common.utils.SecurityUtils;
import com.huike.common.utils.StringUtils;
import com.huike.common.utils.bean.BeanUtils;
import com.huike.common.core.domain.entity.SysUserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
* @author 93238
* @description 针对表【tb_clue(线索)】的数据库操作Service实现
* @createDate 2023-10-12 06:35:46
*/
@Service
public class TbClueServiceImpl extends ServiceImpl<TbClueMapper, TbClue> implements TbClueService{
    @Autowired
    private TbClueMapper clueMapper;
    @Autowired
    private TbActivityService activityService;
    @Autowired
    private SysDictMapper sysDictMapper;
    @Autowired
    private TbAssignRecordService assignRecordService;
    @Autowired
    private TbActivityMapper activityMapper;
    @Autowired
    private TbAssignRecordMapper assignRecordMapper;
    @Autowired
    private SysUserMapper userMapper;
    @Resource
    private TbClueTrackRecordService clueTrackRecordService;
    @Resource
    private TbClueTrackRecordMapper tbClueTrackRecordMapper;

    @Override
    @DataScope(deptAlias = "r", userAlias = "r")
    public List<TbClue> selectList(TbClue clue) {
        return clueMapper.getList(clue);
    }

    /**
     * 批量导入
     */
    @Override
    public Map<String, Integer> addTbClue(List<TbClueExcelVo> cluevoList) {
        List<TbClue> clueList = cluevoList.stream().map(vo -> {
            TbClue tbClue = new TbClue();
            BeanUtils.copyProperties(vo, tbClue);
            tbClue.setCreateBy(SecurityUtils.getUsername());
            tbClue.setCreateTime(DateUtils.getNowDate());
            String activityCode = vo.getActivityCode();
            // 关联活动
            if (StringUtils.isNoneBlank(activityCode)) {
                TbActivity activity = activityService.getOne(new QueryWrapper<TbActivity>().eq("code", activityCode));
                if (activity != null) {
                    tbClue.setActivityId(activity.getId());
                }
            }
            return tbClue;
        }).collect(Collectors.toList());
        return importClues(clueList);
    }

    @Override
    @Transactional
    public Map<String, Integer> importClues(List<TbClue> clueList) {
        if (StringUtils.isNull(clueList) || clueList.size() == 0) {
            throw new CustomException("导入用户数据不能为空！");
        }
        Map<String, Integer> map = new HashMap<>();
        List<TbClue> toAssignlist = new ArrayList<>();
        int successNum = 0;
        int failureNum = 0;
        for (TbClue clue : clueList) {
            try {
                if (StringUtils.isBlank(clue.getPhone())) {
                    failureNum++;
                    continue;
                }
                if (StringUtils.isBlank(clue.getChannel())) {
                    failureNum++;
                    continue;
                }

                // 验证是否存在这个用户
                TbClue dbcule = clueMapper.selectTbClueByPhone(clue.getPhone());
                if (dbcule == null) {
                    // 特殊字段处理
                    String channel = sysDictMapper.selectDictValue(TbClue.ImportDictType.CHANNEL.getDictType(),
                            clue.getChannel());
                    clue.setChannel(channel);

                    if (StringUtils.isNoneBlank(clue.getSubject())) {
                        String subject = sysDictMapper.selectDictValue(TbClue.ImportDictType.SUBJECT.getDictType(),
                                clue.getSubject());
                        clue.setSubject(subject);
                    }

                    if (StringUtils.isNoneBlank(clue.getLevel())) {
                        String level = sysDictMapper.selectDictValue(TbClue.ImportDictType.LEVEL.getDictType(),
                                clue.getLevel());
                        clue.setLevel(level);
                    }

                    if (StringUtils.isNoneBlank(clue.getSex())) {
                        String sex = sysDictMapper.selectDictValue(TbClue.ImportDictType.SEX.getDictType(),
                                clue.getSex());
                        clue.setSex(sex);
                    }

                    if (StringUtils.isNoneBlank(activityService.getClueActivityById(String.valueOf(clue.getActivityId())).getName())) {
                        String sex = sysDictMapper.selectDictValue(TbClue.ImportDictType.SEX.getDictType(),
                                clue.getSex());
                        clue.setSex(sex);
                    }
                    clue.setStatus("1");
                    clueMapper.insert(clue);
                    // 默认分配超级管理员
                    // 如果线索添加成功，利用策略将线索分配给具体的人

                    TbAssignRecord tbAssignRecord = new TbAssignRecord();
                    tbAssignRecord.setAssignId(clue.getId());
                    tbAssignRecord.setUserId(SecurityUtils.getAdmin());
                    tbAssignRecord.setUserName("admin");
                    tbAssignRecord.setDeptId(SecurityUtils.getDeptId());
                    tbAssignRecord.setCreateBy("admin");
                    Date createTime = clue.getCreateTime();
                    tbAssignRecord.setCreateTime(createTime);//TODO这样写可能有问题
                    assignRecordService.save(tbAssignRecord);

                    successNum++;
                    toAssignlist.add(clue);
                } else {
                    failureNum++;
                }
            } catch (Exception e) {
                e.printStackTrace();
                failureNum++;
            }
        }

        map.put("successNum", successNum);
        map.put("failureNum", failureNum);
        return map;
    }

    @Override
    public void add(TbClueDTO tbClueDTO) {
        TbClue tbClue = new TbClue();
        BeanUtils.copyProperties(tbClueDTO, tbClue);
        tbClue.setCreateTime(DateUtils.getNowDate());
        tbClue.setCreateBy(SecurityUtils.getUsername());
        clueMapper.add(tbClue);
    }

    @Override
    public void updateClue(TbClueDTO tbClueDTO) {
        TbClue tbClue = new TbClue();
        BeanUtils.copyProperties(tbClueDTO, tbClue);
        tbClue.setUpdateTime(DateUtils.getNowDate());
        tbClue.setCreateBy(SecurityUtils.getUsername());
        clueMapper.updateById(tbClue);
    }



    @Override
    @DataScope(deptAlias = "d", userAlias = "u")
    public List<TbClue> getPools(TbClue clue) {
        List<TbClue> clueList = clueMapper.getTbClueList(clue);
        clueList.forEach(cl -> {
            if (cl.getActivityId() != null){
                TbActivity activity = activityMapper.getById(cl.getActivityId());
                if (activity != null){
                    cl.setActivityName(activity.getName());
                }
            }
        });
        return clueList;
    }

    @Override
    public TbClue getByClueId(Long id) {
        TbClue clue = clueMapper.getById(id);
        TbActivity activity = activityMapper.getById(clue.getActivityId());

        clue.setActivityInfo(activity.getInfo());
        TbAssignRecord tbAssignRecord = assignRecordMapper.getById(clue.getId());
        clue.setOwner(tbAssignRecord.getCreateBy());
        LocalDateTime time = tbAssignRecord.getCreateTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        clue.setOwnerTime(time);
        return clue;
    }

    @Override
    @Transactional
    public void ListGain(AssignmentVo assignmentVo) {
        Long[] ids = assignmentVo.getIds();
        if (ids != null){
            for (Long id : ids) {
                TbAssignRecord record = assignRecordMapper.getByAssId(id);
                record.setUserName(SecurityUtils.getUsername());
                assignRecordMapper.updateById(record);
                TbClue clue = clueMapper.getById(id);
                clue.setStatus("1");
                clueMapper.updateById(clue);
            }
        }
    }

    @Override
    @Transactional
    public void falseClue(Long id, FalseClueVo clueVo) {
        TbClue clue = clueMapper.getById(id);
        clue.setStatus("4");
        clueMapper.updateById(clue);

        TbClueTrackRecord tbClueTrackRecord = new TbClueTrackRecord();
        tbClueTrackRecord.setCreateBy(SecurityUtils.getUsername());
        tbClueTrackRecord.setCreateTime(Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant()));
        tbClueTrackRecord.setFalseReason(clueVo.getReason());
        tbClueTrackRecord.setRecord(clueVo.getRemark());
        tbClueTrackRecord.setClueId(String.valueOf(id));
        tbClueTrackRecord.setType("1");
        clueTrackRecordService.save(tbClueTrackRecord);

    }


    @Override
    @Transactional
    public void assignment(AssignmentVo assignmentVo) {
        Long[] ids = assignmentVo.getIds();
        if (ids!=null&&ids.length>0){
            for (Long id : ids) {
                //获取分配记录
                TbAssignRecord assignRecord = assignRecordMapper.getById(id);
                //根据用户id获取用户信息
                SysUserDTO user = userMapper.selectUserById(assignmentVo.getUserId());

                assignRecord.setLatest("0");
                //更新
                assignRecordMapper.updateById(assignRecord);

                assignRecord.setLatest("1");
                assignRecord.setUserName(user.getUserName());
                //新增新记录
                assignRecordMapper.insert(assignRecord);

                //获取线索记录
                TbClueTrackRecord record = tbClueTrackRecordMapper.selectByRecordId(id);
                if (record!= null){
                    tbClueTrackRecordMapper.updateById(record);
                    //新增新记录
                    record.setCreateBy(user.getUserName());
                    record.setCreateTime(DateUtils.getNowDate());
                    tbClueTrackRecordMapper.insert(record);
                }



            }
        }

    }
}









