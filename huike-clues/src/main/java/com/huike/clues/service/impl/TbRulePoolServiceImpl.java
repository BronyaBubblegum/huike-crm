package com.huike.clues.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huike.clues.domain.TbRulePool;
import com.huike.clues.domain.dto.TbRulePoolDTO;
import com.huike.clues.service.TbRulePoolService;
import com.huike.clues.mapper.TbRulePoolMapper;
import com.huike.common.dtos.ResponseResult;
import com.huike.common.enums.AppHttpCodeEnum;
import com.huike.common.utils.bean.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author 93238
* @description 针对表【tb_rule_pool(线索池规则)】的数据库操作Service实现
* @createDate 2023-10-12 06:35:46
*/
@Service
public class TbRulePoolServiceImpl extends ServiceImpl<TbRulePoolMapper, TbRulePool>
    implements TbRulePoolService{

    /**
     * 编辑池
     *
     * @param tbRulePoolDTO
     * @return
     */
    @Override
    public ResponseResult editPool(TbRulePoolDTO tbRulePoolDTO) {

        TbRulePool tbRulePool=new TbRulePool();
        BeanUtils.copyProperties(tbRulePoolDTO,tbRulePool);
        if (tbRulePoolDTO.getLimitTime()!=null){
        tbRulePool.setLimitTime(Math.toIntExact(tbRulePoolDTO.getLimitTime()));
        }
        if (tbRulePoolDTO.getWarnTime()!=null){

        tbRulePool.setWarnTime(Math.toIntExact(tbRulePoolDTO.getWarnTime()));
        }
            if (tbRulePoolDTO.getRepeatGetTime()!=null){

        tbRulePool.setRepeatGetTime(Math.toIntExact(tbRulePoolDTO.getRepeatGetTime()));
            }
                if (tbRulePoolDTO.getMaxNunmber()!=null){
        tbRulePool.setMaxNunmber(Math.toIntExact(tbRulePoolDTO.getMaxNunmber()));

                }

        updateById(tbRulePool);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 决定线索或池
     *
     * @param type
     * @return
     */
    @Override
    public ResponseResult getType(String type) {

        LambdaQueryWrapper<TbRulePool> lambdaQueryWrapper=new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(TbRulePool::getType,type);
        List<TbRulePool> list = list(lambdaQueryWrapper);
        if (list.size()>1){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        TbRulePool tbRulePool = list.get(0);
        return ResponseResult.okResult(tbRulePool);
    }
}




