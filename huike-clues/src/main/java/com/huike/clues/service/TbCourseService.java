package com.huike.clues.service;

import com.huike.clues.domain.SysUser;
import com.huike.clues.domain.TbCourse;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huike.clues.domain.dto.TbCourseDTO;
import com.huike.common.core.domain.entity.SysUserDTO;
import com.huike.common.dtos.ResponseResult;

import java.util.List;

/**
* @author 93238
* @description 针对表【tb_course(课程管理)】的数据库操作Service
* @createDate 2023-10-12 06:35:46
*/
public interface TbCourseService extends IService<TbCourse> {

    /**
     * 新增课程
     * @param tbCourse
     * @return
     */
    ResponseResult addCourse(TbCourse tbCourse);

    /**
     * 修改课程
     * @param tbCourse
     * @return
     */
    ResponseResult editCourse(TbCourse tbCourse);

    /**
     * 加课程列表
     * @param dto
     * @return
     */
    List<TbCourse> selectCourseList(TbCourseDTO dto);

    /**
     * 创造下拉课表
     * @param subject
     * @return
     */
    ResponseResult listSelect(String subject);

    /**
     * 删除
     * @param ids
     * @return
     */
    ResponseResult delCourse(String ids);

    /**
     * 通过id得到
     * @param id
     * @return
     */
    ResponseResult getCourseById(Integer id);
}
