package com.huike.clues.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huike.clues.domain.TbClueTrackRecord;
import com.huike.clues.service.TbClueTrackRecordService;
import com.huike.clues.mapper.TbClueTrackRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author 93238
* @description 针对表【tb_clue_track_record(线索跟进记录)】的数据库操作Service实现
* @createDate 2023-10-12 06:35:46
*/
@Service
public class TbClueTrackRecordServiceImpl extends ServiceImpl<TbClueTrackRecordMapper, TbClueTrackRecord>
    implements TbClueTrackRecordService{

    @Autowired
     private TbClueTrackRecordMapper tbClueTrackRecordMapper;

    @Override
    public List<TbClueTrackRecord> getList(Integer clueId) {
        return tbClueTrackRecordMapper.getList(clueId);
    }
}




