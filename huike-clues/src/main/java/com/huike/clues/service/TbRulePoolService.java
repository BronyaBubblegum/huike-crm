package com.huike.clues.service;

import com.huike.clues.domain.TbRulePool;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huike.clues.domain.dto.TbRulePoolDTO;
import com.huike.common.dtos.ResponseResult;

/**
* @author 93238
* @description 针对表【tb_rule_pool(线索池规则)】的数据库操作Service
* @createDate 2023-10-12 06:35:46
*/
public interface TbRulePoolService extends IService<TbRulePool> {

    /**
     * 编辑池
     * @param tbRulePoolDTO
     * @return
     */
    ResponseResult editPool(TbRulePoolDTO tbRulePoolDTO);

    /**
     * 决定线索或池
     * @param type
     * @return
     */
    ResponseResult getType(String type);


}
