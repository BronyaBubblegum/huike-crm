package com.huike.clues.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huike.clues.domain.SysUser;
import com.huike.clues.domain.TbCourse;
import com.huike.clues.domain.dto.TbCourseDTO;
import com.huike.clues.service.TbCourseService;
import com.huike.clues.mapper.TbCourseMapper;
import com.huike.common.core.domain.entity.SysUserDTO;
import com.huike.common.dtos.ResponseResult;
import com.huike.common.enums.AppHttpCodeEnum;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author 93238
 * @description 针对表【tb_course(课程管理)】的数据库操作Service实现
 * @createDate 2023-10-12 06:35:46
 */
@Service
public class TbCourseServiceImpl extends ServiceImpl<TbCourseMapper, TbCourse>
        implements TbCourseService {
    /**
     * 新增课程
     *
     * @param tbCourse
     * @return
     */
    @Override
    public ResponseResult addCourse(TbCourse tbCourse) {
        save(tbCourse);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 修改课程
     *
     * @param tbCourse
     * @return
     */
    @Override
    public ResponseResult editCourse(TbCourse tbCourse) {
        updateById(tbCourse);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 加课程列表
     *
     * @param dto
     * @return
     */
    @Override
    public List<TbCourse> selectCourseList(TbCourseDTO dto) {

        if (dto == null) {
            return null;
        }

        LambdaQueryWrapper<TbCourse> lambdaQueryWrapper = new LambdaQueryWrapper<>();

        if (dto.getApplicablePerson() != null && StringUtils.isNotBlank(dto.getApplicablePerson())) {
            lambdaQueryWrapper.eq(TbCourse::getApplicablePerson, dto.getApplicablePerson());
        }
        if (dto.getCode() != null && StringUtils.isNotBlank(dto.getCode())) {
            lambdaQueryWrapper.like(StringUtils.isNotBlank(dto.getCode()), TbCourse::getCode, dto.getCode());
        }
        String beginTime = (String) dto.getParams().get("beginCreateTime");
        String endTime = (String) dto.getParams().get("endCreateTime");

        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        try {

            if (StringUtils.isNotBlank(beginTime) && StringUtils.isNotBlank(endTime)) {
                Date beginTimedate = ft.parse(beginTime);
                Date endTimedate = ft.parse(endTime);

                if (beginTime != null
                        && endTime != null) {
                    lambdaQueryWrapper.between(TbCourse::getCreateTime, beginTimedate, endTimedate);
                }
            }

        } catch (ParseException e) {
            throw new RuntimeException(e);
        }


        if (dto.getSubject() != null && StringUtils.isNotBlank(dto.getSubject())) {
            lambdaQueryWrapper.eq(StringUtils.isNotBlank(dto.getSubject()), TbCourse::getSubject, dto.getSubject());
        }

        if (dto.getName() != null && StringUtils.isNotBlank(dto.getName())) {
            lambdaQueryWrapper.like(TbCourse::getName, dto.getName());
        }

        String sqlSelect = lambdaQueryWrapper.getSqlSelect();

        System.out.println("sqlSelect = " + sqlSelect);

        List<TbCourse> list = list(lambdaQueryWrapper);

        // if (list==null||list.size()==0){
        //     list=list();
        // }


        return list;
    }

    /**
     * 创造下拉课表
     *
     * @param subject
     * @return
     */
    @Override
    public ResponseResult listSelect(String subject) {
        List<TbCourse> list = new ArrayList<>();

        if (StringUtils.isNotBlank(subject)) {
            LambdaQueryWrapper<TbCourse> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(TbCourse::getSubject, subject);
            list = list(lambdaQueryWrapper);
        }

        return ResponseResult.okResult(list);
    }

    /**
     * 删除
     *
     * @param ids
     * @return
     */
    @Override
    public ResponseResult delCourse(String ids) {

        String[] splits = ids.split(",");
        List<Long> idss = new ArrayList<>();

        for (String split : splits) {
            idss.add(Long.valueOf(split));
        }
        removeByIds(idss);
        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    /**
     * 通过id得到
     *
     * @param id
     * @return
     */
    @Override
    public ResponseResult getCourseById(Integer id) {

        TbCourse byId = getById(id);
        System.out.println("byId = " + byId);
        return ResponseResult.okResult(byId);
    }
}




