package com.huike.clues.service;

import com.huike.clues.domain.TbClueTrackRecord;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author 93238
* @description 针对表【tb_clue_track_record(线索跟进记录)】的数据库操作Service
* @createDate 2023-10-12 06:35:46
*/
public interface TbClueTrackRecordService extends IService<TbClueTrackRecord> {

    List<TbClueTrackRecord> getList(Integer clueId);
}
