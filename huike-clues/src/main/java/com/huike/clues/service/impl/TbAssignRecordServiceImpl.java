package com.huike.clues.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huike.clues.domain.TbAssignRecord;
import com.huike.clues.service.TbAssignRecordService;
import com.huike.clues.mapper.TbAssignRecordMapper;
import org.springframework.stereotype.Service;

/**
* @author 93238
* @description 针对表【tb_assign_record(分配记录表)】的数据库操作Service实现
* @createDate 2023-10-12 06:35:46
*/
@Service
public class TbAssignRecordServiceImpl extends ServiceImpl<TbAssignRecordMapper, TbAssignRecord>
    implements TbAssignRecordService{

}




