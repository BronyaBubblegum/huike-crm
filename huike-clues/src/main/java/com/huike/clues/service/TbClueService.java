package com.huike.clues.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huike.clues.domain.TbClue;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huike.clues.domain.dto.TbClueDTO;
import com.huike.clues.domain.vo.TbClueExcelVo;
import com.huike.clues.domain.vo.AssignmentVo;
import com.huike.clues.domain.vo.FalseClueVo;
import com.huike.clues.domain.vo.TbClueExcelVo;

import java.util.List;
import java.util.Map;

/**
* @author 93238
* @description 针对表【tb_clue(线索)】的数据库操作Service
* @createDate 2023-10-12 06:35:46
*/
public interface TbClueService extends IService<TbClue> {


    List<TbClue> selectList(TbClue clue);


    void add(TbClueDTO tbClueDTO);

    Map<String, Integer> addTbClue(List<TbClueExcelVo> listBatch);
    /**
     * 导入线索数据
     * @param clueList
     * @return 结果
     */
    public Map<String,Integer> importClues(List<TbClue> clueList);
    void updateClue(TbClueDTO tbClueDTO);

    void assignment(AssignmentVo assignmentVo);

    List<TbClue> getPools(TbClue clue);

    TbClue getByClueId(Long id);

    void ListGain(AssignmentVo assignmentVo);

    void falseClue(Long id, FalseClueVo clueVo);

}
