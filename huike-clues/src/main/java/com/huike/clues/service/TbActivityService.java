package com.huike.clues.service;

import com.huike.clues.domain.TbActivity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huike.clues.domain.dto.TbActivityDTO;
import com.huike.clues.domain.dto.TbActivityPageQueryDTO;
import com.huike.clues.result.TableDataInfoActivityList;

import java.util.List;

/**
* @author 93238
* @description 针对表【tb_activity(活动管理)】的数据库操作Service
* @createDate 2023-10-12 06:35:46
*/
public interface TbActivityService extends IService<TbActivity> {

    List<TbActivity> getActivityList(TbActivity tbActivity);

    void deleteByIds(List<Long> ids);

    void addActivity(TbActivity tbActivity);

    TbActivity getClueActivityById(String id);

    List<TbActivity> getActivityListByStatus(String channel);

    void updateActivity(TbActivity tbActivity);

    TbActivity selectTbActivityByCode(String activityCode);
}
