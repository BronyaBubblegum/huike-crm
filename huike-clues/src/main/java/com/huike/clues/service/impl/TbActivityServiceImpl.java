package com.huike.clues.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.huike.clues.domain.TbActivity;
import com.huike.clues.domain.dto.TbActivityDTO;
import com.huike.clues.domain.dto.TbActivityPageQueryDTO;
import com.huike.clues.result.TableDataInfoActivityList;
import com.huike.clues.service.TbActivityService;
import com.huike.clues.mapper.TbActivityMapper;
import com.huike.common.utils.uuid.UUIDUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @author 93238
 * @description 针对表【tb_activity(活动管理)】的数据库操作Service实现
 * @createDate 2023-10-12 06:35:46
 */
@Service
public class TbActivityServiceImpl extends ServiceImpl<TbActivityMapper, TbActivity>
        implements TbActivityService {
    @Autowired
    private TbActivityMapper tbActivityMapper;

    @Override
    public List<TbActivity> getActivityList(TbActivity tbActivity) {
        return tbActivityMapper.getActivityList(tbActivity);
    }

    @Override
    public void deleteByIds(List<Long> ids) {
        tbActivityMapper.deleteBatchIds(ids);
    }

    @Override
    public void addActivity(TbActivity tbActivity) {
        tbActivity.setCreateTime(new Date(System.currentTimeMillis()));
        tbActivity.setCode(UUIDUtils.getUUID());
        tbActivity.setStatus("2");
        tbActivityMapper.addActivity(tbActivity);
    }

    @Override
    public TbActivity getClueActivityById(String id) {
        if (id != null) {
            return tbActivityMapper.getClueActivityById(id);
        }
        return null;
    }

    @Override
    public List<TbActivity> getActivityListByStatus(String channel) {
        return tbActivityMapper.getByChannel(channel);
    }

    @Override
    public void updateActivity(TbActivity tbActivity) {
        tbActivityMapper.updateActivity(tbActivity);
    }

    @Override
    public TbActivity selectTbActivityByCode(String code) {
        return tbActivityMapper.selectTbActivityByCode(code);
    }
}




